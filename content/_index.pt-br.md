---
title: "Manual de Usuário do Briar"
---

# O que é o Briar?

Briar é um aplicativo de mensagens desenvolvido para ativistas, jornalistas e todos aqueles e aquelas que precisam de uma maneira segura, fácil e robusta para se comunicar. Diferente dos aplicativos de mensagens tradicionais, o Briar não depende de um servidor central – as mensagens são sincronizadas diretamente entre os aparelhos dos usuários. Se não houver conexão com a internet, o Briar pode se manter sincronizado através do Bluetooth ou Wi-Fi, mantendo assim a informação fluindo em uma crise. Se houver conexão com a internet, o Briar pode se conectar através da rede Tor, protegendo os usuários e suas relações da vigilância.

# Instalação

O Briar está [disponível na Google Play](https://play.google.com/store/apps/details?id=org.briarproject.briar.android) para os aparelhos Android.

> **Dica:** Se você não tem certeza se o seu aparelho é um aparelho Android, verifique se há nele o aplicativo Play Store. Se houver, então é um Android.

Se você possui um aparelho Android mas prefere não usar a Google Play, o website do Briar tem instruções para você instalar o aplicativo através do [F-Droid](https://briarproject.org/fdroid.html) ou [download direto](https://briarproject.org/apk.html).

# Criando uma conta

![Criando uma conta](/img/create-account.png)

A primeira vez que você abrir o Briar, será pedido para criar uma conta. Você pode escolher qualquer apelido e senha. A senha, entretanto, deve ter pelo menos 8 caracteres e ser difícil de adivinhar. 

> **Aviso:** Sua conta do Briar é armazenada de forma segura no seu próprio aparelho, não na nuvem. Se você desinstalar o Briar ou esquecer sua senha, não há forma de recuperar sua conta.

Toque em "Criar Conta". Quando ela for criada, você será levado para a lista de contatos.

{{< break-do-not-translate >}}

# Adicionando contatos

![Opções para adicionar um contato](/img/add-contact-options-cropped.png)

Para adicionar um contato, toque no ícone de adição no canto inferior direito da sua lista de contatos.

Escolha uma das duas opções que aparecem.

{{< break-do-not-translate >}}

## Adicionar contato à distância

![Adicionando um contato à distância](/img/add-contact-remotely.png)

Copie o link <code>briar://</code> e envie isso para a pessoa que você quer adicionar.
Você também pode usar o botão "Compartilhar" e escolher um aplicativo para enviar o link.

Cole o link que você recebeu da pessoa que quer adicionar no campo de texto abaixo.
Clique em "Continuar" e escolha um apelido para o novo contato.

Em seguida você irá ver a tela de "Solicitações de contatos pendentes"
que lhe informa sobre o estado de cada um de seus contatos pendentes.
O Briar irá tentar se conectar regularmente com o seu contato para adicioná-lo.

Uma vez que a conexão for bem-sucedida, vocês serão adicionados na lista de contatos um do outro.
Parabéns! Você está pronto para se comunicar de maneira segura.

Se o Briar não conseguir se conectar com o seu contato depois de 48 horas, a lista de contatos pendentes mostrará a mensagem "Adicionar o contato falhou". Então ambos devem deletar o contato pendente da lista e adicionar o link um do outro novamente.

{{< break-do-not-translate >}}

![Contatos pendentes](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Adicionar contato próximo

![Adicionando um contato, parte 1](/img/add-contact-1.png)

Uma outra maneira de adicionar um contato é encontrando a pessoa que você quer adicionar pessoalmente. Cada um de vocês irá escanear um código QR da tela um do outro. Isso garante que você está se conectando à pessoa certa, então ninguém mais pode se passar por você ou ler suas mensagens.

Toque em "Continuar" quando você estiver pronto para começar.

{{< break-do-not-translate >}}

![Adicionando um contato, parte 2](/img/add-contact-2.png)

Posicione o código QR do seu contato no visor da câmera. Você terá de esperar alguns segundos pelo foco da câmera.

Quando a câmera tiver escaneado o código QR, você irá ver a mensagem "Esperando pelo contato escanear e conectar". Agora é a vez do seu contato escanear o seu código QR.

{{< break-do-not-translate >}}

![A lista de contatos mostrando o contato recém adicionado](/img/contact-list-with-contact-cropped.png)

Seus aparelhos irão trocar informações, e depois de alguns segundos vocês terão adicionado um ao outro em suas listas de contatos. Parabéns! Você está pronto para se comunicar de maneira segura.

{{< break-do-not-translate >}}

# Mensagens ##

![Uma conversa de mensagens privada](/img/manual_messaging-cropped.png)

Para enviar uma mensagem privada, toque no nome de um contato na lista de contatos.

> **Dica:** Todas as mensagens no Briar são criptografadas de ponta-a-ponta, assim que ninguém mais as podem ler.

Se o seu contato estiver desconectado, sua mensagem será enviada na próxima vez em que ambos estiverem conectados à internet.

{{< break-do-not-translate >}}

# Apresentando Contatos ##

![Começando uma apresentação](/img/introduction-1-cropped.png)

Você pode apresentar seus contatos um ao outro através do Briar. Isso permite que eles se tornem contatos sem a necessidade de se encontrarem pessoalmente. Para começar uma apresentação, toque no nome de um contato na lista de contatos e selecione "Fazer apresentação" a partir do menu.

{{< break-do-not-translate >}}

![Escolhendo o segundo contato](/img/introduction-2-cropped.png)

A seguir, escolha o outro contato que você deseja apresentar.

{{< break-do-not-translate >}}

![Adicionando uma mensagem para ambos os contatos](/img/introduction-3-cropped.png)

Adicione uma mensagem opcional para os contatos, depois toque em "Fazer apresentação"

{{< break-do-not-translate >}}

![Uma solicitação de apresentação](/img/introduction-5-cropped.png)

Seus contatos irão ver uma mensagem perguntando a eles se aceitam ser apresentados. Se ambos aceitarem, eles serão adicionados à lista de contatos um do outro e poderão se comunicar seguramente.

{{< break-do-not-translate >}}

# Grupos Privados ##

![O menu principal mostrando o recurso de grupos privados selecionado](/img/nav-drawer-private-groups.png)

Você pode usar o Briar para  bate-papo em grupo com os seus contatos. Para criar um grupo, abra o menu principal e selecione "Grupos Privados". A lista de grupos privados irá abrir. Toque no ícone de adição para criar um novo grupo.

{{< break-do-not-translate >}}

![Criando um grupo privado, parte 1](/img/create-private-group-1-cropped.png)

Escolha um nome para o seu grupo, depois toque em "Criar grupo".

{{< break-do-not-translate >}}

![Criando um grupo privado, parte 2](/img/create-private-group-2-cropped.png)

Em seguida, escolha os contatos que você gostaria de convidar para se juntar ao grupo.

{{< break-do-not-translate >}}

![Criando um grupo privado, parte 3](/img/create-private-group-3-cropped.png)

Adicione uma mensagem opcional para os contatos escolhidos, depois toque em "Enviar convite". Os contatos verão uma mensagem convidando-os a se juntar ao grupo.

{{< break-do-not-translate >}}

![A lista de grupos privados mostrando o grupo recém criado](/img/create-private-group-4-cropped.png)

O novo grupo será adicionado a sua lista de grupos privados. Essa lista mostra todos os grupos nos quais você participa.

{{< break-do-not-translate >}}

![Uma conversa em um grupo privado](/img/private-group-2.png)

As mensagens em grupos privados são organizadas em tópicos. Qualquer membro pode responder a uma mensagem ou começar um novo tópico.

O criador do grupo é o único que pode convidar novos membros. Qualquer membro pode deixar o grupo. Porém, se o criador deixar, o grupo é dissolvido.

{{< break-do-not-translate >}}

# Fóruns

![O menu principal mostrando o recurso de fóruns](/img/nav-drawer-forums.png)

Um fórum é uma conversa pública. Diferentemente de um grupo privado, qualquer membro do fórum pode convidar seus próprios contatos para entrar nele.

Para criar um fórum, abra o menu principal e selecione "Fóruns". A lista de fóruns irá abrir. Toque no ícone de adição para criar um novo fórum.

{{< break-do-not-translate >}}

![Criando um fórum](/img/create-forum-1-cropped.png)

Escolha um nome para o seu fórum, depois toque em "Criar fórum".

{{< break-do-not-translate >}}

![A lista de fóruns mostrando o fórum recém criado](/img/create-forum-2-cropped.png)

O novo fórum irá ser adicionado a sua lista de fóruns. Essa lista mostra todos os fóruns nos quais você participa.

{{< break-do-not-translate >}}

![Compartilhando um fórum, parte 1](/img/share-forum-1-cropped.png)

Para compartilhar um fórum com os seus contatos, toque no fórum para abri-lo, em seguida toque no ícone de compartilhamente no canto superior direito.

{{< break-do-not-translate >}}

![Compartilhando um fórum, parte 2](/img/share-forum-2-cropped.png)

A seguir, escolha os contatos com os quais você gostaria de compartilhar o fórum. Pressione o ícone de seleção no canto superior direito quando tiver acabado. 

{{< break-do-not-translate >}}

![Compartilhando um fórum, parte 3](/img/share-forum-3-cropped.png)

Adicione uma mensagem opcional para os contatos escolhidos, e então toque em "Compartilhar fórum". Os contatos verão uma mensagem convidando-os para se juntarem ao fórum.

{{< break-do-not-translate >}}

![Uma conversa em um fórum](/img/forum-1.png)

As mensagens nos fóruns são organizadas em tópicos. Qualquer membro pode responder uma mensagem ou começar um novo tópico.

Qualquer membro de um fórum pode convidar novos membros, e qualquer membro, incluindo o criador, pode deixar o fórum. O fórum continua existindo até que o último membro vá embora.

{{< break-do-not-translate >}}

# Blogs

{{< break-do-not-translate >}}

![O menu principal mostrando o recurso de blogs selecionado](/img/nav-drawer-blogs.png)

Sua conta do Briar possui um blog embutido. Você pode usar isso para postar notícias e atualizações sobre a sua vida. Seu blog é automaticamente compartilhado com os seus contatos, e o blog deles é compartilhado com você.

Para ler o blog dos seus contatos ou escrever uma postagem, abra o menu principal e selecione "Blogs".

{{< break-do-not-translate >}}

![Escrevendo uma postagem no blog, parte 1](/img/write-blog-post-1-cropped.png)

Para escrever uma postagem, toque no ícone do lápis no canto superior direito do feed do seu blog.

{{< break-do-not-translate >}}

![Escrevendo uma postagem no blog, parte 2](/img/write-blog-post-2-cropped.png)

Escreva sua postagem e toque em "Publicar".

{{< break-do-not-translate >}}

![O feed do blog mostrando a postagem recém criada](/img/write-blog-post-3-cropped.png)

A sua nova postagem aparecerá no feed do blog.

{{< break-do-not-translate >}}

![Reblogando uma postagem, parte 1](/img/reblog-1-cropped.png)

Para reblogar uma postagem, toque no ícone de reblogar no canto superior direito da postagem.

{{< break-do-not-translate >}}

![Reblogando uma postagem, parte 2](/img/reblog-2-cropped.png)

Adicione um comentário opcional, e então toque em "Reblogar".

{{< break-do-not-translate >}}

![Reblogando uma postagem, parte 3](/img/reblog-3-cropped.png)

A postagem reblogada aparecerá no feed no blog com o seu comentário anexado a ela.

{{< break-do-not-translate >}}

# Feeds RSS

Você pode usar o Briar para ler qualquer blog ou site de notícias que publique um feed RSS. Os artigos são baixados através do Tor para proteger a sua privacidade. Você pode reblogar e comentar os artigos dos feeds RSS, exatamente como faria com as postagens de blog.

> **Dica:** RSS é uma maneira dos websites publicarem artigos de uma forma que é fácil de republicar.

{{< break-do-not-translate >}}

![Importando um feed RSS, parte 1](/img/import-rss-feed-1-cropped.png)

Para importar um feed RSS, abra o feed do blog e selecione "Importar feed RSS" a partir do menu, no canto superior direito.

{{< break-do-not-translate >}}

![Importando um feed RSS, parte 2](/img/import-rss-feed-2-cropped.png)

Introduza o endereço URL do feed RSS e toque em "Importar". Os artigos serão baixados e adicionados ao feed do blog. Isso pode levar alguns minutos.

{{< break-do-not-translate >}}

![O feed do blog mostrando um artigo RSS recém importado](/img/import-rss-feed-3-cropped.png)

Quando novos artigos forem publicados, o Briar irá baixá-los automaticamente. Artigos importados não são compartilhados com os seus contatos, a menos que você os reblogue.

{{< break-do-not-translate >}}

![Gerenciando feeds RSS, parte 1](/img/manage-rss-feeds-1-cropped.png)

Para gerenciar seus feeds RSS, abra o feed do blog e selecione "Gerenciar feeds RSS" a partir do menu, no canto superior direito.

{{< break-do-not-translate >}}

![Gerenciando feeds RSS, parte 2](/img/manage-rss-feeds-2-cropped.png)

Toque no ícone de lixeira para remover um feed. Os respectivos artigos importados serão deletados do feed do blog, exceto aqueles que você tenha reblogado. 

{{< break-do-not-translate >}}

# Deletando Contatos #

![Deletando um contato](/img/delete-contact-cropped.png)

Para deletar um contato, toque no nome do contato na lista de contatos e selecione "Apagar contato" a partir do menu, no canto superior direito.

> **Dica:** Para proteger a sua privacidade, o contato não será notificado de que você o deletou. Ele apenas verá você desconectado de agora em diante.

{{< break-do-not-translate >}}

# Configurações

![A lista de configurações](/img/manual_dark_theme_settings-cropped.png)

Para encontrar as configurações, abra o menu principal e selecione "Configurações"
Aqui você pode customizar sua experiência no Briar.

{{< break-do-not-translate >}}

## Tema

![O menu principal com um tema escuro](/img/manual_dark_theme_nav_drawer-cropped.png)

Você pode modificar o esquema de cores que o Briar usa:

* **Claro:** O Briar usurá cores claras.
* **Escuro:** O Briar usará cores escuras.
* **Automático:** O Briar modificará seu esquema de cores de acordo com a hora do dia.
* **Padrão do sistema:** O Briar usará o esquema de cores do sistema.

{{< break-do-not-translate >}}

## Conectar via Internet (Tor)

![A seção de "Redes" na tela de configurações](/img/manual_tor_settings-cropped.png)

> **Dica:** O Briar usa o Tor para se conectar à internet. Tor é uma rede de computadores operados por voluntários ao redor do mundo para ajudar as pessoas a acessar a internet de maneira privada e sem censura. "Pontes" são computadores que podem ajudar você a se conectar ao Tor se o seu governo ou provedora de internet estiverem bloqueando ele.

Você pode controlar como o Briar se conecta à internet:

* **Automático baseado em sua localização:** O Briar escolherá como se conectar baseado em sua localização atual.
* **Usar o Tor sem pontes:** O Briar se conectará ao Tor sem usar pontes.
* **Usar o Tor com pontes:** O Briar usará pontes para se conectar ao Tor.
* **Não conectar:** O Briar não se conectará à internet de qualquer modo.

## Usar Dados Móveis

Você pode controlar se o Briar usará dados móveis. Se você desabilitar o uso de dados móveis, o Briar apenas usará a internet quando você estiver conecetado ao Wi-Fi.

## Conectar à Internet (Tor) Apenas Quando Carregando

Você pode escolher se o Briar usará a internet enquanto seu aparelho estiver usando a bateria. Se você habilitar essa configuração, o Briar apenas usará a internet quando seu aparelho estiver conecetado à energia.

## Bloqueio de Tela

![A seção de 'Segurança' na tela de configurações](/img/manual_app_lock-cropped.png)

> **Dica:** Esse recurso não está disponível na versão 4 do Android.

Para proteger sua privacidade quando outras pessoas estiverem usando seu aparelho, você pode bloquear o Briar sem deslogar de sua conta. Isso previne que o Briar seja usado até você inserir seu PIN, padrão ou senha. 

O Briar usa o mesmo PIN, padrão, ou senha que você usa normalmente para desbloquear seu aparelho, de modo que essa configuração estará desabilitada se você ainda não escolheu um PIN, padrão ou senha. Você pode usar o aplicativo de Configuração do seu aparelho para escolher um.

![O menu principal mostrando a opção 'Bloquear App'](/img/manual_app_lock_nav_drawer-cropped.png)

Quando a tela de bloqueio estiver habilitada, a opção "Bloquear App" será adicionada ao menu principal do Briar. Você pode usar essa opção para bloquear o Briar sem deslogar de sua conta.

{{< break-do-not-translate >}}

![Desbloqueando o Briar](/img/manual_app_lock_keyguard.png)

Quando o Briar estiver bloqueado, você será perguntando pelo seu PIN, padrão ou senha para desbloqueá-lo.

{{< break-do-not-translate >}}

## Bloqueio de Tela Por Tempo de Inatividade

> **Dica:** Esse recurso não está disponível na versão 4 do Android.

Você pode escolher bloquear o Briar automaticamente quando ele não for usado por um certo período de tempo.
