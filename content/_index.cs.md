---
title: "Uživatelská příručka Briar"
---

# Co je Briar?

Briar je aplikace pro posílání zpráv, navržená pro aktivisty, novináře a kohokoliv dalšího, kdo potřebuje bezpečný, snadný a robustní způsob komunikace. Oproti tradičním aplikacím Briar nezávisí na centrálním serveru - zprávy jsou synchronizovány přímo mezi zařízeními uživatelů. Pokud nefunguje internet, Briar může synchronizovat přes Bluetooth nebo Wi-Fi, čímž umožňuje komunikaci i v období krize. Pokud internet funguje, Briar může synchronizovat pomocí sítě Tor, čímž chrání uživatele a jejich blízké před sledováním.

# Instalace

Briar je [dostupný na Google Play](https://play.google.com/store/apps/details?id=org.briarproject.briar.android) pro zařízení s Androidem.

> **Tip:** Pokud si nejste jisti jestli máte zařízení s Androidem, podívejte se zda najdete aplikaci Obchodu Play. Pokud ji najdete, zařízení používá Android.

Pokud máte Android zařízení, ale nechcete použít Google Play, webová stránka Briaru obsahuje instrukce pro instalaci aplikace pomocí [F-Droid](https://briarproject.org/fdroid.html) nebo [přímé stažení](https://briarproject.org/apk.html).

# Vytváření účtu

![Vytváření účtu](/img/create-account.png)

Když poprvé otevřete Briar, zobrazí se dotaz na vytvoření účtu. Můžete vybrat jakoukoliv přezdívku a heslo. Heslo by mělo být dlouhé alespoň 8 znaků a nemělo by jít uhodnout.

> **Varování:** Váš účet Briar je uložen bezpečně na vašem zařízení, ne v cloudu. Pokud odinstalujete Briar nebo zapomenete své heslo, nebude možné váš účet obnovit.

Klepněte na "Vytvořit účet". Po vytvoření budete přesměrováni do seznamu kontaktů.

{{< break-do-not-translate >}}

# Přidávání kontaktů

![Možnosti přidání kontaktu](/img/add-contact-options-cropped.png)

Pro přidání kontaktu klepněte na ikonu plus v pravém dolním rohu seznamu kontaktů.

Vyberte jednu ze dvou zobrazených možností.

{{< break-do-not-translate >}}

## Přidat vzdálený kontakt

![Přidávání vzdáleného kontaktu](/img/add-contact-remotely.png)

Zkopírujte <code>briar://</code> odkaz a pošlete ho osobě, kterou chcete přidat.
Také můžete použít tlačítko "Sdílet" pro výběr aplikace, která odkaz odešle.

Do následujícího pole vložte odkaz, který obdržíte od osoby, kterou chcete přidat.
Klikněte na "Pokračovat" a vyberte přezdívku pro nový kontakt.

Dále uvidíte obrazovku Čekajících žádostí o kontakt
informuje vás o stavu každého čekajícího kontaktu.
Briar se bude pravidelně pokoušet kontakt spojit a přidat.

Jakmile se spojení naváže, budete přidáni do seznamu kontaktů ostatních.
Gratulujeme! Můžete bezpečně komunikovat.

Pokud se Briaru nepodaří spojit s vaším kontaktem po 48 hodinách, seznam čekajících kontaktů zobrazí zprávu o selhání přidávání kontaktu. Vy i druhý kontakt byste měli smazat ze seznamu čekající kontakt a přidat znovu vzájemně odkazy.

{{< break-do-not-translate >}}

![Čekající kontakty](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Přidat kontakt v okolí

![Přidávání kontaktu, krok 1](/img/add-contact-1.png)

Další možností jak přidat kontakt je osobní setkání s osobou, kterou chcete přidat. Každý oskenuje QR kód toho druhého z jejich obrazovky telefonu. Toto zajistí, že se připojíte opravdu ke správné osobě a nikdo nepovolaný nebude číst vaše zprávy.

Klepněte na "Pokračovat" jakmile budete připraveni začít.

{{< break-do-not-translate >}}

![Přidávání kontaktu, krok 2](/img/add-contact-2.png)

Zamiřte hledáčkem na QR kód vašeho kontaktu. Může být potřeba pár sekund počkat, než fotoaparát zaostří.

Jakmile fotoaparát oskenuje QR kód, uvidíte zprávu "Čekání až kontakt oskenuje kód a připojí se". To je chvíle, kdy by měl váš kontakt oskenovat váš QR kód.

{{< break-do-not-translate >}}

![Seznam kontaktů zobrazuje nově přidaný kontakt](/img/contact-list-with-contact-cropped.png)

Vaše zařízení si vymění informaci a po několika sekundách budete přidáni do seznamů kontaktů ostatních. Gratulujeme! Můžete bezpečně komunikovat.

{{< break-do-not-translate >}}

# Zprávy s ##

![Konverzace soukromými zprávami](/img/manual_messaging-cropped.png)

Pro odeslání soukromé zprávy klepněte na jméno kontaktu v seznamu kontaktů.

> **Tip:** Všechny zprávy v Briaru jsou koncově šifrované, takže je nemůže číst nikdo jiný.

Pokud je váš kontakt offline, vaše zpráva bude doručena jakmile budete oba online.

{{< break-do-not-translate >}}

# Představování kontaktů ##

![Zahájení představení](/img/introduction-1-cropped.png)

Pomocí Briaru můžete představit vzájemně vaše kontakty. Toto umožňuje aby se staly kontakty aniž by se osobně setkaly. Pro zahájení představení klepněte na jméno kontaktu v seznamu kontaktů a v menu vyberte "Představit".

{{< break-do-not-translate >}}

![Vybrání druhého kontaktu](/img/introduction-2-cropped.png)

Dále vyberte jiný kontakt, který chcete představit.

{{< break-do-not-translate >}}

![Přidání zprávy oběma kontaktům](/img/introduction-3-cropped.png)

Přidejte volitelnou zprávu pro kontakty, pak klepněte na "Představit".

{{< break-do-not-translate >}}

![Požadavek představení](/img/introduction-5-cropped.png)

Vaše kontakty uvidí zprávu žádosti o přijetí představení. Pokud oba přijmou, budou přidáni do kontaktů toho druhého a budou moci spolu bezpečně komunikovat.

{{< break-do-not-translate >}}

# Soukromé skupiny ##

![Hlavní nabídka ukazuje vybranou funkci soukromých skupin](/img/nav-drawer-private-groups.png)

Briar můžete používat pro skupinové chaty se svými kontakty. Pro vytvoření skupiny otevřete hlavní menu a vyberte "Soukromé skupiny". Otevře se seznam soukromých skupin. Klepněte na ikonu plus pro vytvoření nové skupiny.

{{< break-do-not-translate >}}

![Vytvoření soukromé skupiny, krok 1](/img/create-private-group-1-cropped.png)

Vyberte název skupiny a klepněte na "Vytvořit skupinu".

{{< break-do-not-translate >}}

![Vytvoření soukromé skupiny, krok 2](/img/create-private-group-2-cropped.png)

Dále vyberte kontakty, které chcete do skupiny pozvat. Stiskněte ikonu zaškrtnutí, jakmile budete hotovi.

{{< break-do-not-translate >}}

![Vytvoření soukromé skupiny, krok 3](/img/create-private-group-3-cropped.png)

Přidejte volitelnou zprávu pro vybrané kontakty, klepněte na "Odeslat pozvánku". Kontakty uvidí zprávu, kde jsou zváni k připojení.

{{< break-do-not-translate >}}

![Seznam soukromých skupin zobrazuje nově vytvořenou skupinu](/img/create-private-group-4-cropped.png)

Nová skupina bude přidána do vašeho seznamu soukromých skupin. Tento seznam obsahuje všechny skupiny, do kterých patříte.

{{< break-do-not-translate >}}

![Konverzace v soukromé skupině](/img/private-group-2.png)

Zprávy v soukromých skupinách patří do vláken. Člen může odpovědět na zprávu a nebo zahájit nové vlákno.

Zakladatel skupiny je jediný, kdo může pozvat nové členy. Kterýkoliv člen může skupinu opustit. Pokud ji opustí zakladatel, skupina je rozpuštěna.

{{< break-do-not-translate >}}

# Fóra

![Hlavní nabídka ukazuje vybranou funkci fór](/img/nav-drawer-forums.png)

Fórum je veřejná konverzace. Na rozdíl od soukromých skupin může kdokoliv, kdo se připojí do fóra pozvat své vlastní kontakty.

Pro vytvoření fóra otevřete hlavní nabídku a vyberte "Fóra". Seznam fór se otevře. Klepněte na ikonu plus pro vytvoření nového fóra.

{{< break-do-not-translate >}}

![Vytvoření fóra](/img/create-forum-1-cropped.png)

Vyberte název vašeho fóra a klepněte na "Vytvořit fórum".

{{< break-do-not-translate >}}

![Seznam fór zobrazuje nově vytvořené fórum](/img/create-forum-2-cropped.png)

Nové fórum bude přidáno do vašeho seznamu kontaktů. Tento seznam zobrazuje všechna fóra, do kterých patříte.

{{< break-do-not-translate >}}

![Sdílení fóra, krok 1](/img/share-forum-1-cropped.png)

Pro sdílení fóra s vašimi kontakty klepněte na fórum, čímž ho otevřete a následně klepněte na ikonu sdílení.

{{< break-do-not-translate >}}

![Sdílení fóra, krok 2](/img/share-forum-2-cropped.png)

Dále vyberte kontakty, se kterými chcete fórum sdílet. Stiskněte ikonu zaškrtnutí, jakmile budete hotovi.

{{< break-do-not-translate >}}

![Sdílení fóra, krok 3](/img/share-forum-3-cropped.png)

Přidejte volitelnou zprávu pro vybrané kontakty, klepněte na "Sdílet fórum". Kontakty uvidí zprávu, kde jsou zváni k připojení.

{{< break-do-not-translate >}}

![Konverzace na fóru](/img/forum-1.png)

Zprávy ve fórech patří do vláken. Člen může odpovědět na zprávu a nebo zahájit nové vlákno.

Jakýkoliv člen fóra může pozvat nové členy a kterýkoliv člen, včetně zakladatele může fórum opustit. Fórum existuje dokud ho neopustí poslední člen.

{{< break-do-not-translate >}}

# Blogy

{{< break-do-not-translate >}}

![Hlavní nabídka ukazuje vybranou funkci blogu](/img/nav-drawer-blogs.png)

Váš Briar účet má vestavěný blog. Můžete ho použít pro zveřejnění novinek a aktualizací z vašeho života. Váš blog je automaticky sdílen s vašimi kontakty a jejich blogy jsou sdíleny s vámi.

Pro čtení blogů vašich kontaktů a pro napsání příspěvku, otevřete hlavní nabídku a vyberte "Blogy".

{{< break-do-not-translate >}}

![Napsání příspěvku blogu, krok 1](/img/write-blog-post-1-cropped.png)

Pro napsání příspěvku klepněte na ikonu pera v horní části kanálu blogu.

{{< break-do-not-translate >}}

![Napsání příspěvku blogu, krok 2](/img/write-blog-post-2-cropped.png)

Napište příspěvek a klepněte na "Zveřejnit".

{{< break-do-not-translate >}}

![Kanál blogu zobrazuje nově vytvořený příspěvek](/img/write-blog-post-3-cropped.png)

Váš nový příspěvek se zobrazí v kanálu blogu.

{{< break-do-not-translate >}}

![Znovu-zveřejňuji příspěvek blogu, krok 1](/img/reblog-1-cropped.png)

Pro znovu-zveřejnění příspěvku klepněte na ikonu znovu-zveřejnění v rohu příspěvku.

{{< break-do-not-translate >}}

![Znovu-zveřejňuji příspěvek blogu, krok 2](/img/reblog-2-cropped.png)

Přidejte volitelný komentář a klepněte na "Znovu-zveřejnit".

{{< break-do-not-translate >}}

![Znovu-zveřejňuji příspěvek blogu, krok 3](/img/reblog-3-cropped.png)

Znovu-zveřejněný příspěvek se objeví v kanálu blogu spolu s vaším připojeným komentářem.

{{< break-do-not-translate >}}

# RSS kanály

Briar můžete použít pro čtení jakéhokoli blogu nebo novinové stránky, která zveřejňuje RSS kanál. Články jsou staženy přes Tor kvůli ochraně vašeho soukromí. Články z RSS kanálů můžete znovu-zveřejnit a komentovat, stejně jako příspěvky blogu.

> **Tip:** RSS je způsob, kterým webové stránky zveřejňují články, aby je bylo možné snadno znovu-zveřejnit.

{{< break-do-not-translate >}}

![Import RSS kanálu, krok 1](/img/import-rss-feed-1-cropped.png)

Pro import RSS kanálu otevřete kanál blogu a vyberte položku nabídky "Importovat RSS kanál".

{{< break-do-not-translate >}}

![Import RSS kanálu, krok 2](/img/import-rss-feed-2-cropped.png)

Zadejte adresu URL RSS kanálu a klepněte na "Import". Články budou staženy a přidáno do kanálu blogu. Toto může trvat několik minut.

{{< break-do-not-translate >}}

![Kanál blogu zobrazuje nově importovaný RSS článek](/img/import-rss-feed-3-cropped.png)

Jakmile jsou zveřejněny nové články, Briar je automaticky stáhne. Importované články nejsou sdíleny s vašimi kontakty pokud nezvolíte jejich znovu-zveřejnění.

{{< break-do-not-translate >}}

![Správa RSS kanálů, krok 1](/img/manage-rss-feeds-1-cropped.png)

Pro správu vašich RSS kanálů otevřete kanál blogu a vyberte položku nabídky "Spravovat RSS kanály".

{{< break-do-not-translate >}}

![Správa RSS kanálů, krok 2](/img/manage-rss-feeds-2-cropped.png)

Klepněte na ikonu koše pro odebrání kanálu. Importované články budou odebrány z kanálu blogu, až na případné články, které jste znovu-zveřejnili.

{{< break-do-not-translate >}}

# Mazání kontaktů #

![Mazání kontaktu](/img/delete-contact-cropped.png)

Pro smazání kontaktu klepněte v seznamu kontaktů na jméno kontaktu a vyberte položku nabídky "Smazat kontakt".

> **Tip:** Z důvodu ochrany vašeho soukromí nebude kontakt upozorněn, že jste ho smazali. Pouze uvidí, že jste offline.

{{< break-do-not-translate >}}

# Nastavení

![Seznam nastavení](/img/manual_dark_theme_settings-cropped.png)

Do nastavení se dostanete tak, že otevřete hlavní nabídku a vyberete "Nastavení".
Pak budete moci přizpůsobit Briar.

{{< break-do-not-translate >}}

## Motiv

![Hlavní nabídka ve tmavém motivu](/img/manual_dark_theme_nav_drawer-cropped.png)

Můžete změnit barevné schéma, které Briar používá:

* **Světlé:** Briar použije světlé barvy.
* **Tmavé:** Briar použije tmavé barvy.
* **Automatické:** Briar změní barevné schéma v závislosti na denním čase.
* **Systémové výchozí:** Briar použije systémové schéma barev.

{{< break-do-not-translate >}}

## Připojit pomocí internetu (Tor)

![Sekce 'Sítě' obrazovky Nastavení](/img/manual_tor_settings-cropped.png)

> **Tip:** Briar používá Tor k připojení k internetu. Tor je síť počítačů, kterou provozují dobrovolníci z celého světa, aby pomohli lidem přistupovat k internetu soukromě a bez cenzury. "Mosty" jsou počítače, které vám mohou pomoci připojit se k Tor i v případě, že ho vaše vláda nebo poskytovatel internetu blokuje.

Můžete ovlivnit jak se Briar připojuje k internetu:

* **Automaticky podle umístění:** Briar vybere metodu připojení v závislosti na vašem aktuálním umístění.
* **Použít Tor bez mostů:** Briar se připojí k Tor bez použití mostů.
* **Použít Tor s mosty:** Briar se připojí k Tor pomocí mostů.
* **Nepřipojovat:** Briar se vůbec nepřipojí k internetu.

## Použít mobilní data

Můžete ovlivnit zda Briar použije mobilní data. Pokud mobilní data vypnete, Briar použije internet pouze pokud se připojíte k Wi-Fi.

## Připojit se přes internet (Tor) pouze při nabíjení

Můžete ovlivnit zda Briar použije internet když vaše zařízení není nabíjeno. Pokud tuto volbu zapnete, Briar použijete internet pouze když je zařízení připojeno do elektrické sítě.

## Zámek obrazovky

![Sekce 'Bezpečnost' obrazovky Nastavení](/img/manual_app_lock-cropped.png)

> **Tip:** Tato funkce není dostupná na Android verze 4.

Pokud vaše zařízení používají ostatní lidé, můžete chránit své soukromí tím, že zamknete Briar, aniž byste se z něho odhlásili. Toto zamezí aby byl Briar použit dokud nezadáte správný PIN, frázi a nebo heslo.

Briar používá stejný PIN, frázi nebo heslo, které běžně používáte pro odemčení zařízení. Tato volba bude vypnuta (zšedne) pokud jste dosud nevybrali PIN, frázi a nebo heslo. Můžete použít aplikaci Nastavení abyste je vybrali.

![Hlavní nabídka zobrazuje volbu 'Zamknout aplikaci'](/img/manual_app_lock_nav_drawer-cropped.png)

Při zapnuté možnosti zamykání obrazovky, bude v hlavní nabídce dostupná volba "Zamknout aplikaci". Tuto volbu použijte pro zamčení Briar, aniž byste byli odhlášeni.

{{< break-do-not-translate >}}

![Odemčení Briar](/img/manual_app_lock_keyguard.png)

Když je Briar zamčen, budete dotázáni na svůj PIN, frázi a nebo heslo pro odemčení.

{{< break-do-not-translate >}}

## Doba neaktivity zámku obrazovky

> **Tip:** Tato funkce není dostupná na Android verze 4.

Můžete zvolit, aby byl Briar automaticky zamčen, pokud nebude používán po určenou dobu.
