---
title: "Manual de usuario de Briar"
---

# ¿Qué es Briar?

“Briar es una aplicación de mensajería diseñada para activistas, periodistas y cualquier otra persona que necesite una forma segura, sencilla y sólida de comunicarse. A diferencia de las aplicaciones de mensajería tradicionales, Briar no depende de un servidor central: los mensajes se sincronizan directamente entre los dispositivos de los usuarios. Si no hay conexión a Internet, Briar puede sincronizarse a través de Bluetooth o Wi-Fi, lo que permite que la información siga fluyendo en caso de crisis. Si no hay conexión a Internet, Briar puede sincronizarse a través de la red Tor, lo que protege a los usuarios y sus relaciones de la vigilancia.”

# Instalación

Briar está [disponible en Google Play](https://play.google.com/store/apps/details?id=org.briarproject.briar.android) para dispositivos Android.

> **Tip:** Si no estás seguro de tener un dispositivo Android, verifica si tienes la aplicación Play Store. Si es así, ejecuta Android..

Si tienes un dispositivo Android pero prefieres no usar Google Play, el sitio web de Briar tiene instrucciones para instalar la aplicación vía. [F-Droid](https://briarproject.org/fdroid.html) o [direct download](https://briarproject.org/apk.html).

# Crear una cuenta

![Crear una cuenta](/img/create-account.png)

La primera vez que abras Briar, te solicitará crear una cuenta. Puedes escoger cualquier apodo y contraseña. La contraseña debe tener al menos 8 caracteres de largo y ser difícil de adivinar.

> **Cuidado** Tu cuenta de Briar se almacena de forma segura en tu dispositivo, no en la nube. Si desinstalas Briar y/u olvidas tu contraseña, no hay forma de recuperar tu cuenta.

Toca "Crear cuenta". Cuando se haya creado tu cuenta, serás llevado a la lista de contactos.

{{< break-do-not-translate >}}

# Añadiendo Contactos

![Options for adding a contact](/img/add-contact-options-cropped.png)

Para agregar un contacto, toca el ícono de más en la parte inferior derecha de la lista de contactos.

Elige una de las dos opciones que aparecen.

{{< break-do-not-translate >}}

## Agrega un contacto a distancia

![Agregando un contacto a distancia](/img/add-contact-remotely.png)

Copia el <code>briar://</code>envía el enlace a la persona que quieres agregar
También puedes usar el botón "Compartir" para elegir una aplicación para enviar el enlace

Pega el enlace que recibes de la persona que quieres agregar en el campo de texto de abajo. Haz clic en “Continuar” y elige un apodo para el nuevo contacto.

A continuación, verás una pantalla de “Solicitudes de Contacto Pendientes”
que te informa sobre el estado de cada contacto pendiente.
Briar intentará conectarse regularmente a tu contacto para añadirlo.

Una vez que la conexión tenga éxito, serás añadido a la lista de contactos del otro.
¡Felicidades! Estás listo para comunicarte de manera segura.

Si Briar no puede conectarse a tu contacto después de 48 horas, la lista de contactos pendientes mostrará el mensaje “La adición del contacto ha fallado”. Ambos deben eliminar el contacto pendiente de la lista y añadir los enlaces del otro nuevamente.

{{< break-do-not-translate >}}

![Contactos Pendientes](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Agregar contacto cercano

![Agregando el contacto, paso 1](/img/add-contact-1.png)

Otra forma de agregar un contacto es reunirse con la persona que deseas agregar. Cada uno de ustedes escaneará un código QR de la pantalla de la otra persona. Esto garantiza que te estás conectando con la persona adecuada, para que nadie más pueda hacerse pasar por ti o leer tus mensajes.

Toca "Continuar" cuando estés listo para comenzar.

{{< break-do-not-translate >}}

![Agregando contacto, paso 2](/img/add-contact-2.png)

Alinea el código QR de tu contacto en el visor. Puede que tengas que esperar unos segundos para que la cámara enfoque.

“Cuando la cámara haya escaneado el código QR, verás el mensaje "Esperando que el contacto escanee y se conecte". Ahora tu contacto debe escanear tu código QR.

{{< break-do-not-translate >}}

![La lista de contactos muestra un contacto recién añadido](/img/contact-list-with-contact-cropped.png)

Tus dispositivos intercambiarán información, y después de unos segundos, serás añadido a la lista de contactos del otro. ¡Felicidades! Estás listo para comunicarte de manera segura.

{{< break-do-not-translate >}}

# Mensajería ##

![Una conversación de mensajería privada](/img/manual_messaging-cropped.png)

Para enviar un mensaje privado, toca el nombre de un contacto en la lista de contactos.

> **Tip:** Todos los mensajes en Briar están cifrados de extremo a extremo, por lo que nadie más puede leerlos.

Si tu contacto está desconectado, tu mensaje será entregado la próxima vez que ambos estén en línea.

{{< break-do-not-translate >}}

# Agregando Contactos ##

![Comenzando una introducción](/img/introduction-1-cropped.png)

Puedes agregar tus contactos entre sí a través de Briar. Esto les permite convertirse en contactos sin tener que encontrarse. Para iniciar una presentación, toca el nombre de un contacto en la lista de contactos y elige “Hacer presentación” en el menú.

{{< break-do-not-translate >}}

![Eligiendo el segundo contacto](/img/introduction-2-cropped.png)

A continuación, elige el otro contacto que quieres agregar.

{{< break-do-not-translate >}}

![Agregar un mensaje a ambos contactos](/img/introduction-3-cropped.png)

Agrega un mensaje opcional para los contactos, luego toca “Hacer introducción”.

{{< break-do-not-translate >}}

![Una solicitud de introducción](/img/introduction-5-cropped.png)

Tus contactos verán un mensaje preguntándoles si aceptan la presentación. Si ambos aceptan, se agregarán a las listas de contactos del otro y podrán comunicarse de manera segura.

{{< break-do-not-translate >}}

# Grupos Privados ##

![El menú principal muestra la función de grupos privados seleccionada](/img/nav-drawer-private-groups.png)

Puedes usar Briar para chats grupales con tus contactos. Para crear un grupo, abre el menú principal y elige “Grupos Privados”. Se abrirá la lista de grupos privados. Toca el icono de más para crear un nuevo grupo.

{{< break-do-not-translate >}}

![Creando un grupo privado, paso 1](/img/create-private-group-1-cropped.png)

Elige un nombre para tu grupo, luego toca "Crear grupo".

{{< break-do-not-translate >}}

![Creando un grupo privado, paso 2](/img/create-private-group-2-cropped.png)

A continuación, elige los contactos que te gustaría invitar a unirse al grupo. Presiona el icono de la marca de verificación cuando hayas terminado.

{{< break-do-not-translate >}}

![Creando un grupo privado, paso 3](/img/create-private-group-3-cropped.png)

Agrega un mensaje opcional a los contactos elegidos, luego toca “Enviar Invitación”. Los contactos verán un mensaje invitándolos a unirse.

{{< break-do-not-translate >}}

![La lista de grupos privados muestra un grupo recién creado](/img/create-private-group-4-cropped.png)

El nuevo grupo se añadirá a tu lista de grupos privados. Esta lista muestra todos los grupos a los que perteneces.

{{< break-do-not-translate >}}

![Una conversación en un grupo privado](/img/private-group-2.png)

Los mensajes en grupos privados están organizados en hilos. Cualquier miembro puede responder a un mensaje o iniciar un nuevo hilo.

El creador del grupo es el único que puede invitar a nuevos miembros. Cualquier miembro puede abandonar el grupo. Si el creador abandona el grupo, éste se disuelve.

{{< break-do-not-translate >}}

# Foros

![El menú principal muestra la función de foros seleccionada](/img/nav-drawer-forums.png)

Un foro es una conversación pública. A diferencia de un grupo privado, cualquiera que se una a un foro puede invitar a sus propios contactos.

Para crear un foro, abre el menú principal y elige “Foros”. Se abrirá la lista de foros. Toca el icono de más para crear un nuevo foro.

{{< break-do-not-translate >}}

![Creando un foro](/img/create-forum-1-cropped.png)

Elige un nombre para tu foro, luego toca “Crear Foro”.

{{< break-do-not-translate >}}

![La lista de foros muestra un foro recién creado](/img/create-forum-2-cropped.png)

El nuevo foro se agregará a tu lista de foros. Esta lista muestra todos los foros a los que perteneces.

{{< break-do-not-translate >}}

![Compartiendo un foro, paso 1](/img/share-forum-1-cropped.png)

Para compartir un foro con tus contactos, toca el foro para abrirlo, luego toca el icono de compartir.

{{< break-do-not-translate >}}

![Compartiendo un foro, paso 2](/img/share-forum-2-cropped.png)

A continuación, elige los contactos con los que te gustaría compartir el foro. Presiona el icono de la marca de verificación cuando hayas terminado.

{{< break-do-not-translate >}}

![Compartiendo un foro, paso 3](/img/share-forum-3-cropped.png)

Añade un mensaje opcional a los contactos seleccionados, luego toca “Compartir Foro”. Los contactos verán un mensaje invitándolos a unirse.

{{< break-do-not-translate >}}

![Una conversación en un foro](/img/forum-1.png)

Los mensajes en los foros están organizados en hilos. Cualquier miembro puede responder a un mensaje o iniciar un nuevo hilo.

Cualquier miembro de un foro puede invitar a nuevos miembros y cualquier miembro, incluido el creador, puede abandonar el foro. El foro continúa existiendo hasta que el último miembro lo abandone.

{{< break-do-not-translate >}}

# Blogs

{{< break-do-not-translate >}}

![El menú principal muestra la función de blogs seleccionada](/img/nav-drawer-blogs.png)

Tu cuenta de Briar tiene un blog incorporado. Puedes usarlo para publicar noticias y actualizaciones sobre tu vida. Tu blog se comparte automáticamente con tus contactos, y los blogs de ellos se comparten contigo.

Para leer los blogs de tus contactos o escribir una publicación, abre el menú principal y selecciona “Blogs”.

{{< break-do-not-translate >}}

![Escribiendo una publicación de blog, paso 1](/img/write-blog-post-1-cropped.png)

Para escribir una publicación, toca el ícono de la pluma en la parte superior del feed del blog.

{{< break-do-not-translate >}}

![Escribiendo una publicación de blog, paso 2](/img/write-blog-post-2-cropped.png)

Escribe tu publicación y toca “Publicar”.

{{< break-do-not-translate >}}

![El feed del blog muestra una nueva publicación creada](/img/write-blog-post-3-cropped.png)

Tu nueva publicación aparecerá en el feed del blog.

{{< break-do-not-translate >}}

![Reposteando una publicación, paso 1](/img/reblog-1-cropped.png)

Para repostear una publicación, toca el ícono de reposteo en la esquina de la publicación.

{{< break-do-not-translate >}}

![Reposteando una publicación, paso 2](/img/reblog-2-cropped.png)

Agrega un comentario opcional y toca “Repostear”.

{{< break-do-not-translate >}}

![Reposteando una publicación, paso 3](/img/reblog-3-cropped.png)

La publicación reposteada aparecerá en el feed del blog con tu comentario adjunto.

{{< break-do-not-translate >}}

# RSS Feeds

Puedes usar Briar para leer cualquier blog o sitio de noticias que publique un feed RSS. Los artículos se descargan a través de Tor para proteger tu privacidad. Puedes rebloguear y comentar en los artículos de los feeds RSS, al igual que puedes hacerlo con las publicaciones de blogs.

> **Tip:** RSS es una forma en que los sitios web pueden publicar artículos en un formato que es fácil de republicar.

{{< break-do-not-translate >}}

![Importando un feed RSS, paso 1](/img/import-rss-feed-1-cropped.png)

Para importar una fuente RSS, abre la fuente del blog y elige "Importar fuente RSS" en el menú.

{{< break-do-not-translate >}}

![Importando un feed RSS, paso 2](/img/import-rss-feed-2-cropped.png)

Ingresa la URL del feed RSS y pulsa “Importar”. Los artículos serán descargados y añadidos al feed del blog. Esto puede tardar unos minutos.

{{< break-do-not-translate >}}

![El feed del blog mostrando un artículo RSS recién importado](/img/import-rss-feed-3-cropped.png)

Cuando se publiquen nuevos artículos, Briar los descargará automáticamente. Los artículos importados no se compartirán con tus contactos a menos que elijas compartirlos.

{{< break-do-not-translate >}}

![Manejando las fuentes de RSS, paso 1](/img/manage-rss-feeds-1-cropped.png)

Para gestionar tus feeds de RSS, abre el feed del blog y selecciona “Gestionar Feeds de RSS” del menú.

{{< break-do-not-translate >}}

![Manejando las fuentes de RSS, paso 2](/img/manage-rss-feeds-2-cropped.png)

Pulsa el ícono de la papelera para eliminar un feed. Los artículos importados se eliminarán del feed del blog, excepto los artículos que hayas vuelto a publicar.

{{< break-do-not-translate >}}

# Eliminando Contactos #

![Eliminando un contacto](/img/delete-contact-cropped.png)

Para eliminar un contacto, toca el nombre del contacto en la lista de contactos y elige “Eliminar contacto” en el menú.

> **Tip:** Para proteger tu privacidad, el contacto no será notificado de que lo has eliminado. Simplemente te verán como desconectado de ahora en adelante.

{{< break-do-not-translate >}}

# Ajustes

![La lista de ajustes](/img/manual_dark_theme_settings-cropped.png)

Para encontrar las configuraciones, abre el menú principal y selecciona “Configuraciones”. Aquí puedes personalizar tu experiencia con Briar.

{{< break-do-not-translate >}}

## Tema

![El menú principal en un tema oscuro.](/img/manual_dark_theme_nav_drawer-cropped.png)

Puedes cambiar el esquema de colores que utiliza Briar:

* **Light:** Briar utilizará colores claros.
* **Dark:** Briar usará colores oscuros.
* **Automatic:** Briar cambiará su combinación de colores según la hora del día.
* **System default:** Briar utilizará el esquema de colores del sistema.

{{< break-do-not-translate >}}

## Conéctate a través de Internet (Tor)

![La sección ‘Redes’ de la pantalla de configuraciones](/img/manual_tor_settings-cropped.png)

> **Tip:** Briar usa Tor para conectarse a internet. Tor es una red de computadoras administrada por voluntarios alrededor del mundo para ayudar a las personas a acceder a internet de manera privada y sin censura. Los “puentes” son computadoras que pueden ayudarte a conectarte a Tor si tu gobierno o proveedor de internet lo está bloqueando.

Puedes controlar cómo Briar se conecta a internet:

* **Automático según la ubicación:** Briar elegirá cómo conectarse basándose en tu ubicación actual.
* **Usa Tor sin puentes:** Briar se conectará a Tor sin usar puentes.
* **Utilice Tor con puentes:** Briar usará puentes para conectarse a Tor.
* **No te conectes:** Briar no se conecta a Internet en absoluto.

## Usar datos móviles

Puedes controlar si Briar usa datos móviles. Si apagas los datos móviles, Briar solo usará internet cuando estés conectado a Wi-Fi.

## Conéctate a Internet (Tor) solo cuando estés cargando.

Puedes controlar si Briar usa internet mientras tu dispositivo funciona con batería. Si activas esta configuración, Briar solo usará internet cuando tu dispositivo esté conectado a la corriente.

## Bloqueo de pantalla

![La sección 'Seguridad' de la pantalla de configuración](/img/manual_app_lock-cropped.png)

> **Tip:** Esta función no está disponible en la versión 4 de Android.

Para proteger tu privacidad cuando otras personas estén usando tu dispositivo, puedes bloquear Briar sin cerrar sesión. Esto evita que Briar se use hasta que ingreses un PIN, patrón o contraseña.

Briar utiliza el mismo PIN, patrón o contraseña que normalmente usas para desbloquear tu dispositivo, por lo que esta configuración estará deshabilitada (en gris) si aún no has elegido un PIN, patrón o contraseña. Puedes usar la aplicación de Configuración de tu dispositivo para elegir uno.

![El menú principal que muestra la opción 'Bloquear aplicación'](/img/manual_app_lock_nav_drawer-cropped.png)

Cuando se activa la configuración de bloqueo de pantalla, se añade una opción de “Bloquear App” al menú principal de Briar. Puedes usar esta opción para bloquear Briar sin cerrar sesión.

{{< break-do-not-translate >}}

![Desbloqueando Briar](/img/manual_app_lock_keyguard.png)

Cuando Briar esté bloqueado, se te pedirá tu PIN, patrón o contraseña para desbloquearlo.

{{< break-do-not-translate >}}

## Tiempo de inactividad antes del bloqueo de pantalla

> **Tip:** Esta función no está disponible en la versión 4 de Android.

Puedes elegir bloquear Briar automáticamente cuando no se haya utilizado durante un cierto período de tiempo.
