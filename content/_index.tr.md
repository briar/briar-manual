---
title: "Briar Kullanıcı Rehberi"
---

# Briar Nedir?

Briar aktivistler ve gazeteciler başta olmak üzere güvenli, kolay ve sağlam bir iletişim isteyen herkes için tasarlanmış bir ileti sistemidir. Geleneksel ileti sistemlerinin aksine Briar merkezi bir sunucu kullanmaz, iletiler doğrudan kullanıcıların aygıtları arasında eşleştirilir. Briar, eğer İnternet yoksa Bluetooth veya Wi-Fi aracılığıyla da iletileri iletebilir, böylece kriz durumlarında bilgi akışını sürdürür. İnternet varken Briar Tor ağı aracılığıyla iletim sağlar, böylece kullanıcıları ve ilişkilerini gözetimden korumuş olur.  

# Kurulum

Briar Android çalıştıran aygıtlar için [Google Play üzerinde bulunabilir](https://play.google.com/store/apps/details?id=org.briarproject.briar.android).

> **Not:** Kullandığınız aygıtın Android çalıştırdığından emin değilseniz, Play Store uygulaması var mı diye bakın. Varsa Android ile çalışıyordur.

Bir Android aygıtınız varsa ama Google Play kullanmak istemiyorsanız, uygulamayı [F-Droid](https://briarproject.org/fdroid.html) üzerinden kurmak için yönergeleri Briar sitesinde bulabilir ya da [doğrudan indiriebilirsiniz] (https://briarproject.org/apk.html).

# Hesap Oluşturmak

![Hesap oluşturmak](/img/create-account.png)

Briar ilk kez açıldığında sizden bir hesap oluşturmanızı ister. Herhangi bir takma ad ve parola seçebilirsiniz. Parola en az 8 karakter uzunluğunda ve öngörülmesi zor olmalıdır.

> **Uyarı:** Briar hesabınız herhangi bir sunucuda değil, aygıtınızda güvenli bir şekilde tutulur. Briar uygulamasını kaldırırsanız ya da parolanızı unutursanız, hesabınızı kurtarmanın bir yolu yoktur.

"Hesap oluştur" üzerine dokunun. Hesabınız oluşturulduğunda kişi listesine yönlendirilirsiniz.

{{< break-do-not-translate >}}

# Kişileri Eklemek

![Kişi ekleme seçenekleri](/img/add-contact-options-cropped.png)

Bir kişi eklemek için, kişi listesinin sağ alt köşesindeki artı simgesine dokunun.

Görüntülenecek iki seçenekten birini seçin.

{{< break-do-not-translate >}}

## Uzaktaki bir kişiyi eklemek

![Uzaktaki bir kişiyi eklemek](/img/add-contact-remotely.png)

<code>briar://</code> bağlantısını kopyalayın ve eklemek istediğiniz kişiye gönderin.
Bağlantıyı göndermek üzere bir uygulama seçmek için "Paylaş" düğmesini de kullanabilirsiniz.

Eklemek istediğiniz kişiden gelen bağlantıyı aşağıdaki metin kutusuna yapıştırın.
"İlerle" üzerine tıklayın ve yeni kişinize bir takma ad verin.

Ardından her bekleyen kişinin durumunu görebileceğiniz
"Bekleyen kişi istekleri" ekranını göreceksiniz.
Briar listeye eklemek için aralıklarla kişilerinize bağlanmayı dener.

Bir bağlantı kurulduğunda, birbirinizin kişi listesine eklenirsiniz.
Tebrikler! Güvenli bir şekilde iletişim kurmaya hazırsınız.

Briar kişinizle 48 saat sonra hala bağlantı kuramamışsa, bekleyen kişiler listesinde "Kişi eklenemedi" iletisi görüntülenir. İkiniz de bekleyen kişiyi listeden silmeli ve birbirinizin bağlantılarını yeniden eklemelisiniz.

{{< break-do-not-translate >}}

![Bekleyen kişiler](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Yakındaki bir kişiyi eklemek

![Bir kişi eklemek, 1. adım](/img/add-contact-1.png)

Kişi eklemenin başka bir yolu eklemek istediğiniz kişiyle fiziksel olarak buluşmanızdır. Her biriniz diğerinin ekranından bir kare kodunu tarayacak. Bu da doğru kişiyi eklediğinizi garanti eder. Böylece başka hiç kimse sizin kılığınıza giremez veya iletilerinizi okuyamaz.

Başlamaya hazır olduğunuzda "İlerle" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir kişi eklemek, 2. adım](/img/add-contact-2.png)

Kişinizin kare kodunu ekranda vurgulanan alan içine getirin. Kameranın odaklanması için bir kaç saniye beklemeniz gerekebilir.

Kameranız kare kodu taradıktan sonra "Kişinin kodu taraması ve bağlantı kurması bekleniyor" iletisini göreceksiniz. Şimdi eklemek istediğiniz kişinin, sizin kare kodunuzu taraması gerekiyor.

{{< break-do-not-translate >}}

![Yeni eklenmiş kişinin göründüğü kişi listesi](/img/contact-list-with-contact-cropped.png)

Aygıtlarınız karşılıklı olarak bilgileri paylaşacak ve bir kaç saniye sonra birbirinizin kişi listesine eklenmiş olacaksınız. Tebrikler! Güvenli şekilde iletişim kurmaya hazırsınız. 

{{< break-do-not-translate >}}

# İleti göndermek ve almak ##

![Özel ileti yazışması](/img/manual_messaging-cropped.png)

Özel ileti göndermek için kişi listesindeki bir kişinin adına dokunun.

> **Not:** Tüm Briar iletileri uçtan uca şifrelendiğinden başka hiç kimse onları okuyamaz.

Kişiniz çevrim dışı ise, iletiniz daha sonra kişiniz çevrim içi olduğunda aktarılır.

{{< break-do-not-translate >}}

# Kişileri tanıştırmak ##

![Tanıştırmayı başlatmak](/img/introduction-1-cropped.png)

Kişilerinizi Briar üzerinden birbiriyle tanıştırabilirsiniz. Böylece fiziksel olarak buluşmalarına gerek kalmadan birbirleriyle bağlantı kurabilirler. Bir tanıştırma başlatmak için kişi listesinde bir kişiye dokunun ve menüden "Tanıştır" üzerine dokunun.

{{< break-do-not-translate >}}

![İkinci kişiyi seçmek](/img/introduction-2-cropped.png)

Ardından, tanıştırmak istediğiniz diğer kişiyi seçin.

{{< break-do-not-translate >}}

![İki kişiye de gönderilecek bir ileti eklemek](/img/introduction-3-cropped.png)

İsteğe bağlı olarak kişilere gönderilecek bir ileti ekleyip, "Tanıştır" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir tanıştırma isteği](/img/introduction-5-cropped.png)

Kişilerinize tanıştırma isteğini kabul edip etmediklerini soran bir ileti görüntülenir. İsteği ikisi de kabul ederse, birbirlerinin kişi listelerine eklenirler ve güvenli olarak iletişim kurabilirler.

{{< break-do-not-translate >}}

# Özel gruplar ##

![Özel gruplar özelliğinin seçilmiş olarak göründüğü ana menü](/img/nav-drawer-private-groups.png)

Kişilerinizle grup sohbetleri yapmak için Briar kullanabilirsiniz. Bir grup oluşturmak için, ana menüyü açın ve "Özel gruplar" üzerine dokunun. Özel gruplar listesi açılır. Yeni bir grup oluşturmak için artı simgesine dokunun.

{{< break-do-not-translate >}}

![Bir özel grup oluşturmak, 1. adım](/img/create-private-group-1-cropped.png)

Grubunuza bir ad verdikten sonra "Grup oluştur" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir özel grup oluşturmak, 2. adım](/img/create-private-group-2-cropped.png)

Ardından gruba davet etmek istediğiniz kişileri seçin. Seçmeyi tamamladığınızda onay simgesine dokunun. 

{{< break-do-not-translate >}}

![Bir özel grup oluşturmak, 3. adım](/img/create-private-group-3-cropped.png)

İsteğe bağlı olarak, seçtiğiniz kişilere gönderilecek bir ileti ekleyin ve ardından "Daveti gönder" üzerine dokunun. Kişilerinize onları gruba katılmaya davet eden bir ileti görüntülenir.

{{< break-do-not-translate >}}

![Yeni oluşturulmuş bir grubun göründüğü özel grup listesi](/img/create-private-group-4-cropped.png)

Yeni grup, özel grup listenize eklenir. Bu listede üyesi olduğunuz tüm gruplar görüntülenir.

{{< break-do-not-translate >}}

![Özel bir gruptaki bir yazışma](/img/private-group-2.png)

Özel gruplardaki iletiler konular altında toplanır. Herhangi bir üye bir iletiye yanıt yazabilir ya da yeni bir konu başlatabilir.

Yalnızca grubu oluşturan kişi gruba yeni üyeler davet edebilir. İsteyen her üye gruptan ayrılabilir. Grubu oluşturan kişi gruptan ayrılırsa grup dağılır. 

{{< break-do-not-translate >}}

# Forumlar

![Forumlar özelliğinin seçilmiş olarak göründüğü ana menü](/img/nav-drawer-forums.png)

Bir forum herkese açık bir yazışmadır. Özel grupların aksine, foruma katılan herkes kendi kişilerini de davet edebilir.

Bir forum oluşturmak için ana menüyü açın ve "Forumlar" üzerine dokunun. Forum listesi açılır. Yeni bir forum oluşturmak için artı simgesine dokunun.

{{< break-do-not-translate >}}

![Bir forum oluşturmak](/img/create-forum-1-cropped.png)

Forumunuza bir ad verin ve ardından "Forum oluştur" üzerine dokunun.

{{< break-do-not-translate >}}

![Yeni oluşturulmuş bir forumun göründüğü forum listesi](/img/create-forum-2-cropped.png)

Yeni forum, forum listenize eklenir. Bu listede, bulunduğunuz tüm forumlar görüntülenir.

{{< break-do-not-translate >}}

![Bir forumu paylaşmak, 1. adım](/img/share-forum-1-cropped.png)

Bir forumu kişilerinizle paylaşmak için önce forumu dokunarak açın ve paylaş simgesine dokunun.

{{< break-do-not-translate >}}

![Bir forumu paylaşmak, 2. adım](/img/share-forum-2-cropped.png)

Ardından forumu paylaşmak istediğiniz kişileri seçin. Seçmeyi tamamlandığınızda onay simgesine dokunun.

{{< break-do-not-translate >}}

![Bir forumu paylaşmak, 3. adım](/img/share-forum-3-cropped.png)

İsteğe bağlı olarak, seçtiğiniz kişilere gönderilecek bir ileti ekleyin ve ardından "Forumu paylaş" üzerine dokunun. Kişilerinize onları foruma katılmaya davet eden bir ileti görüntülenir.

{{< break-do-not-translate >}}

![Bir forumdaki yazışma](/img/forum-1.png)

Forumlardaki iletiler konular altında toplanır. Herhangi bir üye bir iletiye yanıt yazabilir ya da yeni bir konu başlatabilir. 

Forumdaki her üye yeni üyeler davet edebilir ve forumu oluşturan kişi de içinde olmak üzere, isteyen her üye forumdan ayrılabilir. Forum son üye ayrılana kadar varlığını sürdürür.

{{< break-do-not-translate >}}

# Günlükler

{{< break-do-not-translate >}}

![Günlükler özelliğinin seçilmiş olarak göründüğü ana menü](/img/nav-drawer-blogs.png)

Briar hesabınızda bir günlük bulunur. Günlüğü yaşantınızla ilgili haberleri veya güncellemeleri göndermek için kullanabilirsiniz. Günlüğünüz kişilerinizle, onların günlükleri de otomatik olarak sizinle paylaşılır.

Kişilerinizin günlüklerini okumak ya da bir yazı yazmak için, ana menüyü açın ve "Günlükler" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir günlük yazısı yazmak, 1. adım](/img/write-blog-post-1-cropped.png)

Bir yazı yazmak için, günlük akışının üstündeki kalem simgesine dokunun.

{{< break-do-not-translate >}}

![Bir günlük yazısı yazmak, 2. adım](/img/write-blog-post-2-cropped.png)

Yazınızı yazın ve "Yayınla" üzerine dokunun.

{{< break-do-not-translate >}}

![Yeni oluşturulmuş bir yazının göründüğü günlük akışı](/img/write-blog-post-3-cropped.png)

Yeni yazınız günlük akışında görüntülenir.

{{< break-do-not-translate >}}

![Bir günlük yazısını yeniden yayınlamak, 1. adım](/img/reblog-1-cropped.png)

Bir günlük yazısını yeniden yayınlamak için, yazının köşesindeki yeniden yayınla simgesine dokunun. 

{{< break-do-not-translate >}}

![Bir günlük yazısını yeniden yayınlamak, 2. adım](/img/reblog-2-cropped.png)

İsteğe bağlı olarak bir yorum ekleyin ve "Yeniden yayınla" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir günlük yazısını yeniden yayınlamak, 3. adım](/img/reblog-3-cropped.png)

Yeniden yayınladığınız günlük yazısı günlük akışında yorumunuz eklenmiş olacak görüntülenir.

{{< break-do-not-translate >}}

# RSS akışları

Bir RSS akışı yayınlayan herhangi bir günlük veya haber sitesini okumak için de Briar kullanabilirsiniz. Gizliliğinizi korumak için yazılar Tor ile indirilir. RSS akışlarındaki yazıları tıpkı günlük yazılarında olduğu gibi yeniden yayınlayabilir ve yorum ekleyebilirsiniz. 

> **Not:** RSS, sitelerin yazılarını kolayca yeniden yayınlanabilecek şekilde yayınlaması için kullanılan bir yöntemdir.

{{< break-do-not-translate >}}

![Bir RSS akışını içe aktarmak, 1. adım](/img/import-rss-feed-1-cropped.png)

Bir RSS akışını içe aktarmak için günlük akışını açın ve menüden "RSS akışını içe aktar" üzerine dokunun.

{{< break-do-not-translate >}}

![Bir RSS akışını içe aktarmak, 2. adım](/img/import-rss-feed-2-cropped.png)

RSS akışının adresini yazın ve "İçe aktar" üzerine dokunun. Yazılar indirilir ve günlük akışına eklenir. Bu işlemin tamamlanması bir kaç dakika sürebilir.

{{< break-do-not-translate >}}

![Yeni içe aktarılmış bir RSS yazısının göründüğü günlük akışı](/img/import-rss-feed-3-cropped.png)

Yeni yazılar yayınlandığında, Briar onları otomatik olarak indirir. İçe aktarılan yazılar, onları yeniden yayınlamadığınız sürece kişilerinizle paylaşılmaz.

{{< break-do-not-translate >}}

![RSS akışlarını yönetmek, 1. adım](/img/manage-rss-feeds-1-cropped.png)

RSS akışlarınızı yönetmek için günlük akışını açın ve menüden "RSS akışları yönetimi" üzerine dokunun.

{{< break-do-not-translate >}}

![RSS akışlarını yönetmek, 2. adım](/img/manage-rss-feeds-2-cropped.png)

Bir akışı silmek için çöp kutusu simgesine dokunun. Yeniden yayınladıklarınız dışında içe aktarılan yazılar günlük akışından kaldırılır.

{{< break-do-not-translate >}}

# Kişileri silmek #

![Bir kişiyi silmek](/img/delete-contact-cropped.png)

Bir kişiyi silmek için, kişi listesinde kişinin adına dokunun ve menüden "Kişiyi sil" üzerine dokunun.

> **Not:** Gizliliğinizi korumak için onları sildiğiniz kişilere bildirilmez. Sildiğiniz kişiler sizi çevrim dışı olarak görürler.

{{< break-do-not-translate >}}

# Ayarlar

![Ayarlar listesi](/img/manual_dark_theme_settings-cropped.png)

Ayarları bulmak için, ana menüyü açın ve "Ayarlar" üzerine dokunun.
Bu bölümden Briar deneyiminizi özelleştirebilirsiniz.

{{< break-do-not-translate >}}

## Tema

![Koyu tema ile görüntülenen ana menü](/img/manual_dark_theme_nav_drawer-cropped.png)

Briar tarafından kullanılacak renk tamasını seçebilirsiniz:

* **Açık** Briar açık renkleri kullanır.
* **Koyu:** Briar koyu renkleri kullanır.
* **Otomatik:** Briar renk temasını günün saatine göre değiştirir.
* **Sistem varsayılanı:** Briar sistemin renk temasını kullanır.

{{< break-do-not-translate >}}

## İnternet ile bağlantı (Tor)

![Ayarlar ekranının 'Ağlar' bölümü](/img/manual_tor_settings-cropped.png)

> **Not:** Briar İnternet bağlantısı kurmak için Tor kullanır. Tor, insanlara gizlilik sağlayarak ve sansürü aşarak İnternet erişimi sunan, dünya çapındaki gönüllüler tarafından işletilen bilgisayarların oluşturduğu bir ağdır. "Köprüler", hükümetinizin veya İnternet hizmeti sağlayıcınızın engellediği durumlarda Tor ağına bağlanmanıza yardımcı olan bilgisayarlardır.

Briar uygulamasının İnternet bağlantısını nasıl kuracağını ayarlayabilirsiniz:

* **Konuma göre otomatik:** Briar nasıl bağlantı kuracağını bulunduğunuz konuma göre seçer.
* ** Tor köprüler olmadan kullanılsın:** Briar köprü kullanmadan Tor ağı ile bağlantı kurar.
* **Tor köprüler ile kullanılsın:** Briar Tor ağı ile bağlantı kurmak için köprüler kullanır.
* **İnternet bağlantısı kurulmasın:** Briar herhangi bir internet bağlantısı kurmaz.

## Mobil veri kullanımı

Briar tarafından mobil veri kullanılıp kullanılmayacağını ayarlayabilirsiniz. Mobil veriyi kapatırsanız, Briar yalnızca Wi-Fi bağlantısı varken İnternet erişimi sağlar.

## Yalnızca şarjdayken İnternet (Tor) ile bağlantı kur

Aygıtınız pille çalışırken Briar tarafından İnternet kullanıp kullanmayacağını ayarlayabilirsiniz. Bu ayarı açarsanız Briar yalnızca aygıtınız şarja bağlıyken İnternet kullanır.

## Ekran kilidi

![Ayarlar ekranının 'Güvenlik' bölümü](/img/manual_app_lock-cropped.png)

> **Not:** Bu özellik Android 4 sürümünde bulunmaz.

Aygıtınızı başkaları da kullanıyorsa, gizliliğinizi korumak için oturumu kapatmadan Briar uygulamasını kilitleyebilirsiniz. Bu özellik bir PIN ya da parola yazılana kadar Briar kullanılmasını engeller. 

Briar normalde aygıtınızda kullandığınız PIN, parola ya da deseni kullanır. Dolayısıyla bir PIN, parola ya da desen ayarlamadıysanız bu ayar etkin olmaz (grileşir). Aygıtın güvenlik ayarlarından bunları ayarlayabilirsiniz.

!["Uygulama kilidi" seçeneğinin göründüğü ana menü](/img/manual_app_lock_nav_drawer-cropped.png)

Ekran kilidi etkinleştirildiğinde Briar ana menüsüne "Uygulamayı kilitle" seçeneği eklenir. Bu seçeneği kullanarak oturumunuzu kapatmadan Briar uygulamasını kilitleyebilirsiniz.

{{< break-do-not-translate >}}

![Briar kilidini açmak](/img/manual_app_lock_keyguard.png)

Briar kilitlendiğinde, kilidi açmak için PIN, parola ya da desen istenir.

{{< break-do-not-translate >}}

## Ekran kilidi bekleme süresi

> **Not:** Bu özellik Android 4 sürümünde bulunmaz.

Briar belirli bir süre kullanılmadığında otomatik olarak kilitlenecek şekilde ayarlayabilirsiniz.
