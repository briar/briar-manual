---
title: "Manual de utilização do Briar"
---

# O que é o Briar?

Briar é uma aplicação de mensagens concebida para ativistas, jornalistas e qualquer outra pessoa que necessite de uma forma segura, fácil e robusta de comunicar. Ao contrário das aplicações de mensagens tradicionais, o Briar não depende de um servidor central - as mensagens são sincronizadas diretamente entre os dispositivos dos utilizadores. Se a Internet não estiver disponível, o Briar pode sincronizar-se através de Bluetooth, Wi-Fi ou cartões de memória, mantendo a informação a fluir numa crise. Se a Internet estiver disponível, o Briar pode sincronizar-se através da rede Tor, protegendo os utilizadores e as relações entre eles da vigilância.

# Instalação

Briar está [disponível no Google Play] (https://play.google.com/store/apps/details?id=org.briarproject.briar.android) para dispositivos com Android.

> **Dica:** se não tiver a certeza de que tem um dispositivo Android, verifique se tem a aplicação Play Store. Se tiver, é um dispositivo Android.

Se tiver um dispositivo Android mas preferir não utilizar o Google Play, o site do Briar tem instruções para instalar a aplicação através de [F-Droid](https://briarproject.org/fdroid.html) ou [descarregamento direto](https://briarproject.org/apk.html).

# Criar uma conta

![Criar uma conta](/img/create-account.png)

Na primeira vez que abrir o Briar, ser-lhe-á pedido que crie uma conta. Pode escolher qualquer nome de utilizador e palavra-passe. A palavra-passe deve ter pelo menos 8 caracteres e ser difícil de adivinhar.

> **Aviso:** a sua conta Briar é armazenada de forma segura no seu dispositivo, não na nuvem. Se desinstalar o Briar ou se se esquecer da sua palavra-passe, não há forma de recuperar a sua conta.

Toque em "Criar conta". Quando a sua conta tiver sido criada, será levado para a lista de contactos.

{{< break-do-not-translate >}}

# Adicionar contactos

![Opções para adicionar um contacto](/img/add-contact-options-cropped.png)

Para adicionar um contacto, toque no ícone de mais no canto inferior direito da lista de contactos.

Escolha uma das duas opções que aparecem.

{{< break-do-not-translate >}}

## Adicionar contacto à distância

![Adicionar um contacto à distância](/img/add-contact-remotely.png)

Copie a hiperligação <code>briar://</code> e envie-a para a pessoa que pretende adicionar.
Também pode utilizar o botão "Partilhar" para escolher uma aplicação para enviar a hiperligação.

Cole a hiperligação que recebeu da pessoa que pretende adicionar no campo de texto abaixo.
Clique em "Continuar" e escolha uma alcunha para o novo contacto.

Em seguida, verá um ecrã "Pedidos de contacto pendentes"
que informa sobre o estado de cada contacto pendente.
O Briar tentará conectar-se regularmente aos seus contactos para os adicionar.

Quando a conexão for bem sucedida, serão adicionados às listas de contactos um do outro.
Parabéns! Está pronto para comunicar em segurança.

Se Briar não conseguir estabelecer a conexão com o seu contacto após 48 horas, a lista de contactos pendentes mostrará a mensagem "A adição do contacto falhou". Ambos devem eliminar o contacto pendente da lista e adicionar novamente as hiperligações um do outro.

{{< break-do-not-translate >}}

![Contactos pendentes](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Adicionar contacto nas proximidades

![Adicionar um contacto, passo 1](/img/add-contact-1.png)

Outra forma de adicionar um contacto é encontrar-se com a pessoa que pretende adicionar. Cada um de vocês irá ler um código QR do ecrã da outra pessoa. Isto garante que está a estabelecer a conexão com a pessoa certa, para que mais ninguém se possa fazer passar por si ou ler as suas mensagens.

Toque em "Continuar" quando estiver pronto para começar.

{{< break-do-not-translate >}}

![Adicionar um contacto, passo 2](/img/add-contact-2.png)

Alinhe o código QR do seu contacto no visor. Poderá ter de esperar alguns segundos até a câmara focar.

Quando a câmara tiver lido o código QR, verá a mensagem "A aguardar que o contacto digitalize e estabeleça a conexão". Agora, o seu contacto deve digitalizar o seu código QR.

{{< break-do-not-translate >}}

![A lista de contactos mostra um contacto recentemente adicionado](/img/contact-list-with-contact-cropped.png)

Os vossos dispositivos trocam informações e, após alguns segundos, são adicionados às listas de contactos um do outro. Parabéns! Está pronto para comunicar em segurança.

{{< break-do-not-translate >}}

# Mensagens ##

![Uma conversa por mensagem privada](/img/manual_messaging-cropped.png)

Para enviar uma mensagem privada, toque no nome de um contacto na lista de contactos.

> **Dica:** todas as mensagens no Briar são encriptadas de ponta a ponta, pelo que mais ninguém as pode ler.

Se o seu contacto estiver offline, a sua mensagem será entregue da próxima vez que ambos estiverem online.

{{< break-do-not-translate >}}

# Apresentando contactos ##

![Começar uma apresentação](/img/introduction-1-cropped.png)

Pode apresentar os seus contactos uns aos outros através do Briar. Isto permite-lhes tornarem-se contactos sem terem de se encontrar. Para iniciar uma apresentação, toque no nome de um contacto na lista de contactos e escolha "Fazer apresentação" no menu.

{{< break-do-not-translate >}}

![Escolher o segundo contacto](/img/introduction-2-cropped.png)

Em seguida, escolha o outro contacto que pretende apresentar.

{{< break-do-not-translate >}}

![Adicionar uma mensagem para ambos os contactos](/img/introduction-3-cropped.png)

Adicione uma mensagem opcional aos contactos e, em seguida, toque em "Fazer apresentação".

{{< break-do-not-translate >}}

![Um pedido de apresentação](/img/introduction-5-cropped.png)

Os seus contactos verão uma mensagem a perguntar-lhes se aceitam a apresentação. Se ambos aceitarem, serão adicionados às listas de contactos um do outro e poderão comunicar de forma segura.

{{< break-do-not-translate >}}

# Grupos privados ##

![O menu principal mostra a funcionalidade de grupos privados selecionada](/img/nav-drawer-private-groups.png)

Pode utilizar o Briar para conversas de grupo com os seus contactos. Para criar um grupo, abra o menu principal e selecione "Grupos privados". A lista de grupos privados abrir-se-á. Toque no ícone de mais para criar um novo grupo.

{{< break-do-not-translate >}}

![Criar um grupo privado, passo 1](/img/create-private-group-1-cropped.png)

Escolha um nome para o seu grupo e, em seguida, toque em "Criar grupo".

{{< break-do-not-translate >}}

![Criar um grupo privado, passo 2](/img/create-private-group-2-cropped.png)

Em seguida, escolha os contactos que pretende convidar para se juntarem ao grupo. Prima o ícone da marca de verificação quando tiver terminado.

{{< break-do-not-translate >}}

![Criar um grupo privado, passo 3](/img/create-private-group-3-cropped.png)

Adicione uma mensagem opcional aos contactos escolhidos e, em seguida, toque em "Enviar convite". Os contactos verão uma mensagem a convidá-los a aderir.

{{< break-do-not-translate >}}

![A lista de grupos privados que mostra um grupo recentemente criado](/img/create-private-group-4-cropped.png)

O novo grupo será adicionado à sua lista de grupos privados. Esta lista mostra todos os grupos a que pertence.

{{< break-do-not-translate >}}

![Uma conversa num grupo privado](/img/private-group-2.png)

As mensagens nos grupos privados são organizadas em tópicos. Qualquer membro pode responder a uma mensagem ou iniciar um novo tópico.

O criador do grupo é a única pessoa que pode convidar novos membros. Qualquer membro pode abandonar o grupo. Se o criador sair, o grupo é dissolvido.

{{< break-do-not-translate >}}

# Fóruns

![O menu principal mostra a funcionalidade de fóruns selecionada](/img/nav-drawer-forums.png)

Um fórum é uma conversa pública. Ao contrário de um grupo privado, qualquer pessoa que se junte a um fórum pode convidar os seus próprios contactos.

Para criar um fórum, abra o menu principal e selecione "Fóruns". A lista de fóruns abrir-se-á. Toque no ícone de mais para criar um novo fórum.

{{< break-do-not-translate >}}

![Criar um fórum](/img/create-forum-1-cropped.png)

Escolha um nome para o seu fórum e, em seguida, toque em "Criar fórum".

{{< break-do-not-translate >}}

![A lista de fóruns mostra um fórum recentemente criado](/img/create-forum-2-cropped.png)

O novo fórum será adicionado à tua lista de fóruns. Esta lista mostra todos os fóruns a que pertence.

{{< break-do-not-translate >}}

![Partilhar um fórum, passo 1](/img/share-forum-1-cropped.png)

Para partilhar um fórum com os seus contactos, toque no fórum para o abrir e, em seguida, toque no ícone de partilha.

{{< break-do-not-translate >}}

![Partilhar um fórum, passo 2](/img/share-forum-2-cropped.png)

Em seguida, escolha os contactos com os quais pretende partilhar o fórum. Prima o ícone da marca de verificação quando tiver terminado.

{{< break-do-not-translate >}}

![Partilhar um fórum, passo 3](/img/share-forum-3-cropped.png)

Adicione uma mensagem opcional aos contactos escolhidos e, em seguida, toque em "Partilhar fórum". Os contactos verão uma mensagem a convidá-los a participar.

{{< break-do-not-translate >}}

![Uma conversa num fórum](/img/forum-1.png)

As mensagens nos fóruns são organizadas em tópicos. Qualquer membro pode responder a uma mensagem ou iniciar um novo tópico.

Qualquer membro de um fórum pode convidar novos membros, e qualquer membro, incluindo o criador, pode abandonar o fórum. O fórum continua a existir até que o último membro saia.

{{< break-do-not-translate >}}

# Blogues

{{< break-do-not-translate >}}

![O menu principal mostra a função de blogues selecionada](/img/nav-drawer-blogs.png)

A sua conta Briar tem um blogue incorporado. Pode utilizá-lo para publicar notícias e atualizações sobre a sua vida. O seu blogue é automaticamente partilhado com os seus contactos, e os blogues deles são partilhados consigo.

Para ler os blogues dos seus contactos ou escrever uma publicação, abra o menu principal e escolha "Blogues".

{{< break-do-not-translate >}}

![Escrever uma publicação no blogue, passo 1](/img/write-blog-post-1-cropped.png)

Para escrever uma publicação, toque no ícone da caneta na parte superior do feed do blogue.

{{< break-do-not-translate >}}

![Escrever uma publicação no blogue, passo 2](/img/write-blog-post-2-cropped.png)

Escreva a sua publicação e toque em "Publicar".

{{< break-do-not-translate >}}

![O feed do blogue que mostra uma publicação recentemente criada](/img/write-blog-post-3-cropped.png)

A sua nova publicação aparecerá no feed do blogue.

{{< break-do-not-translate >}}

![Reblogar uma publicação, passo 1](/img/reblog-1-cropped.png)

Para reblogar uma publicação, toque no ícone de reblogar no canto da publicação.

{{< break-do-not-translate >}}

![Reblogar uma publicação, passo 2](/img/reblog-2-cropped.png)

Adicione um comentário opcional e toque em "Reblogar".

{{< break-do-not-translate >}}

![Reblogar uma publicação, passo 3](/img/reblog-3-cropped.png)

A publicação reblogada aparecerá no feed do blogue com o seu comentário anexado.

{{< break-do-not-translate >}}

# Feeds RSS

Pode utilizar o Briar para ler qualquer blogue ou site de notícias que publique um feed RSS. Os artigos são descarregados através do Tor para proteger a sua privacidade. Pode reblogar e comentar artigos de feeds RSS, tal como faz com as publicações de blogues.

> **Dica:** o RSS é uma forma dos sites publicarem artigos num formato fácil de republicar.

{{< break-do-not-translate >}}

![Importar um feed RSS, passo 1](/img/import-rss-feed-1-cropped.png)

Para importar um feed RSS, abra o feed do blogue e escolha "Importar feed RSS" no menu.

{{< break-do-not-translate >}}

![Importar um feed RSS, passo 2](/img/import-rss-feed-2-cropped.png)

Introduza o URL do feed RSS e toque em "Importar". Os artigos serão descarregados e adicionados ao feed do blogue. Isto pode demorar alguns minutos.

{{< break-do-not-translate >}}

![O feed do blogue que mostra um artigo RSS recentemente importado](/img/import-rss-feed-3-cropped.png)

Quando são publicados novos artigos, o Briar transfere-os automaticamente. Os artigos importados não são partilhados com os seus contactos, a menos que opte por reblogá-los.

{{< break-do-not-translate >}}

![Gerir feeds RSS, passo 1](/img/manage-rss-feeds-1-cropped.png)

Para gerir os seus feeds RSS, abra o feed do blogue e escolha "Gerir feeds RSS" no menu.

{{< break-do-not-translate >}}

![Gerir feeds RSS, passo 2](/img/manage-rss-feeds-2-cropped.png)

Toque no ícone do lixo para remover um feed. Os artigos importados serão removidos do feed do blogue, exceto os artigos que tenha reblogado.

{{< break-do-not-translate >}}

# Eliminar contactos #

![Eliminar um contacto](/img/delete-contact-cropped.png)

Para eliminar um contacto, toque no nome do contacto na lista de contactos e escolha "Eliminar contacto" no menu.

> **Dica:** para proteger a sua privacidade, o contacto não será notificado de que o eliminou. A partir de agora, apenas o verão a si como offline.

{{< break-do-not-translate >}}

# Definições

![A lista de configurações](/img/manual_dark_theme_settings-cropped.png)

Para encontrar as configurações, abra o menu principal e escolha "Configurações".
Aqui pode personalizar a sua experiência Briar.

{{< break-do-not-translate >}}

## Tema

![O menu principal com um tema escuro](/img/manual_dark_theme_nav_drawer-cropped.png)

Pode alterar o esquema de cores que o Briar usa:

* **Claro:** o Briar usará cores claras.
* **Escuro:** o Briar usará cores escuras.
* **Automático:** o Briar muda o seu esquema de cores consoante a hora do dia.
* **Predefinição do sistema:** o Briar usará o esquema de cores do sistema.

{{< break-do-not-translate >}}

## Conectar via Internet (Tor)

![A secção 'Redes' do ecrã de configurações](/img/manual_tor_settings-cropped.png)

> **Dica:** o Briar utiliza o Tor para estabelecer a conexão com a Internet. O Tor é uma rede de computadores gerida por voluntários em todo o mundo para ajudar as pessoas a aceder à Internet de forma privada e sem censura. As "pontes" são computadores que podem ajudá-lo a estabelecer a conexão com o Tor se o seu governo ou fornecedor de Internet o estiver a bloquear.

Pode controlar como o Briar se conecta à Internet:

* **Automático baseado na localização:** o Briar escolherá como estabelecer a conexão com base na sua localização atual.
* **Utilizar a rede Tor sem pontes:** o Briar estabelecerá a conexão com o Tor sem usar pontes.
* **Utilizar a rede Tor com pontes:** o Briar usará pontes para estabelecer a conexão com o Tor.
* **Não conectar à Internet:** o Briar não estabelece qualquer ligação à Internet.

## Usar dados móveis

Pode controlar se o Briar utiliza dados móveis. Se desativar os dados móveis, o Briar só utilizará a Internet quando estiver conectado por Wi-Fi.

## Conectar à Internet (Tor) apenas quando o dispositivo estiver a carregar

Pode controlar se o Briar utiliza a Internet enquanto o seu dispositivo está a funcionar com bateria. Se ativar esta configuração, o Briar só utilizará a Internet quando o seu dispositivo estiver ligado à corrente.

## Bloqueador de ecrã

![A secção de 'Segurança' no ecrã de configurações](/img/manual_app_lock-cropped.png)

> **Dica:** esta funcionalidade não está disponível na versão 4 do Android.

Para proteger a sua privacidade quando outras pessoas estão a utilizar o seu dispositivo, pode bloquear o Briar sem terminar a sessão. Isto impede que o Briar seja utilizado até que introduza um PIN, um padrão ou uma palavra-passe.

O Briar utiliza o mesmo PIN, padrão ou palavra-passe que utiliza normalmente para desbloquear o dispositivo, pelo que esta configuração será desativada (a cinzento) se ainda não tiver escolhido um PIN, padrão ou palavra-passe. Pode utilizar a aplicação Configurações do seu dispositivo para escolher um.

![O menu principal mostrando a opção 'Bloquear aplicação](/img/manual_app_lock_nav_drawer-cropped.png)

Quando a configuração de bloqueio de ecrã estiver ativada, será adicionada uma opção "Bloquear aplicação" ao menu principal do Briar. Pode utilizar esta opção para bloquear o Briar sem sair do sistema.

{{< break-do-not-translate >}}

![Desbloquear o Briar](/img/manual_app_lock_keyguard.png)

Quando o Briar estiver bloqueado, ser-lhe-á pedido o seu PIN, padrão ou palavra-passe para o desbloquear.

{{< break-do-not-translate >}}

## Tempo limite de inatividade do bloqueio de ecrã

> **Dica:** esta funcionalidade não está disponível na versão 4 do Android.

Pode optar por bloquear o Briar automaticamente quando este não for utilizado durante um determinado período de tempo.
