---
title: "Briar 用戶教學手冊"
---

# Briar 是什麼?

Briar 適合社會行動者、新聞記者以及需要安全、簡易又強大通訊方式來使用的通訊軟體，它不像傳統的通訊應用需依賴中央集中式的伺服器--其訊息可以直接同步於用戶的設備上。一旦網際網路斷線，Briar 還可以繼續透過藍牙或 Wi-Fi 來保持資訊在危急狀況下的流通。在有網際網路的狀況下，Briar 可透由 Tor 網路進行同步保護用戶與其通訊對象免遭監控。

# 安裝

Briar 可自 [Google Play 取得](https://play.google.com/store/apps/details?id=org.briarproject.briar.android) 安裝於執行 Android 系統的設備.

> **提示:** 若您不能確定設備是否為 Android 系統，請查看它是否有 Play Store 應用，若有則即為 Android 設備

若您手上有 Android 設備但不願使用 Google Play，則 Briar 官網提供了如何利用 [F-Droid](https://briarproject.org/fdroid.html) 或 [直接下載](https://briarproject.org/apk.html)的教學指南

# 創建帳號

![創建帳號](/img/create-account.png)

第一次使用 Briar 系統將會要求您先創建一個帳號，您可挑選自己的暱稱和密碼，而密碼應不少於 8 個字元且不易猜到。

> **警告:** 您的 Briar 帳號已安全地儲存於您的設備上而不是雲端。 一旦移除 Briar 或是忘記密碼，則無法再回復原帳號。

輕觸"創建帳號"，一旦帳號順利建立，接下來就會被帶到聯絡人清單

{{< break-do-not-translate >}}

# 增添聯絡人

![增添聯絡人之選項](/img/add-contact-options-cropped.png)

欲增添聯絡人，請輕觸聯絡人清單右下方之圖標

在出現的兩個選項挑選其一

{{< break-do-not-translate >}}

## 添加遠處的聯絡人

![增添遠程聯絡人](/img/add-contact-remotely.png)

複製 <code>briar://</code> 鏈接並將此資訊提供給您欲增添的聯絡人
您可使用 "分享"按鍵來選取欲傳送此鏈接的應用程式

在下方的文字欄位中貼上您從欲添加的聯絡人所取得的鏈接
點擊"繼續"並為新聯絡人選定暱稱

接下來您會看到"待處理聯絡人請求"畫面
它會告知每一則待辦聯絡人的情況
Briar 將會定期試圖聯繫您的聯絡人以將其加入聯絡人列表

成功連接後，您即可互相添增到彼此的聯絡人列表
恭喜! 您現在可以安全地通訊

若 Briar 無法在 48小時內連接上您的聯絡人，待處理的聯絡人列表將會顯示"增添聯絡人失敗"之訊息。雙方都得自清單上刪除待處理聯絡人並再重新加入鏈接 

{{< break-do-not-translate >}}

![待處理的聯絡人](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## 添加附近的聯絡人

![增添聯絡人, 步驟1](/img/add-contact-1.png)

另一種新增聯絡人的作法是實地與對方碰面，彼此互掃設備上的二維碼，如此可確保您與正確的人接洽，別人無法假冒或讀取您的訊息。

準備好了就請輕觸"繼續"開始

{{< break-do-not-translate >}}

![增添聯絡人, 步驟 2](/img/add-contact-2.png)

在鏡頭取景器中對準聯絡人的二維碼，可能要稍等幾秒鐘讓相機對焦

當相機完成二維碼掃瞄後會出現以下訊息 "等候聯絡人掃瞄與連接"，接下來對方就可以掃瞄您的二維碼。 

{{< break-do-not-translate >}}

![聯絡人清單會顯示新增的聯絡人](/img/contact-list-with-contact-cropped.png)

您們的設備將會交流資訊，稍候便可以互加到雙方的聯絡人列表。恭喜！您們之後就能進行安全地通訊了。

{{< break-do-not-translate >}}

# 傳訊 ##

![私密的傳訊對話](/img/manual_messaging-cropped.png)

要傳送私人訊息，請輕點聯絡人清單上的姓名

> **提示:** Briar 全部訊息皆為端對端加密，故其它人無法讀取。

若對方已離線，則您的訊息將會待雙方都在線時再送達

{{< break-do-not-translate >}}

# 引介聯絡人 ##

![開始引介](/img/introduction-1-cropped.png)

您可透由 Briar 來介紹聯絡人，讓他們不必見面就可以相互設為通訊聯絡人。要啟動介紹功能，請從選單中的"進行介紹"中輕點聯絡人列表中的姓名。

{{< break-do-not-translate >}}

![選取第二位聯絡人](/img/introduction-2-cropped.png)

下一步，選取欲介紹之聯絡人

{{< break-do-not-translate >}}

![傳新訊息給兩位聯絡人](/img/introduction-3-cropped.png)

新增可選式訊息給聯終絡人，然後再輕觸"進行介紹"

{{< break-do-not-translate >}}

![介紹請求](/img/introduction-5-cropped.png)

您的聯絡人會收到一則詢問是否同意介紹的訊息，假若同意，則可互增入彼此的聯絡人列表，之後就可以利用 Briar 安全通訊

{{< break-do-not-translate >}}

# 私人小組 ##

![主選單顯示挑選的私人小組功能](/img/nav-drawer-private-groups.png)

您可利用 Briar 與聯絡人進行群組對話。要創建小組，請自主選單中選取"私人小組"以開啟私人小組列表，輕點 + 圖標即可建立新的小組。 

{{< break-do-not-translate >}}

![建立私人小組, 步驟 1](/img/create-private-group-1-cropped.png)

為小組命名後，再輕點"建立小組"

{{< break-do-not-translate >}}

![建立私人小組, 步驟 2](/img/create-private-group-2-cropped.png)

下一步選取欲邀請加入小組的聯絡人，完成後請按住勾選符號即可

{{< break-do-not-translate >}}

![建立私人小組, 步驟 3](/img/create-private-group-3-cropped.png)

對所選取的聯絡人發送額外訊息，然後輕觸"發送邀請"，對方會收到邀請訊息。

{{< break-do-not-translate >}}

![私人小組列表顯示新建立的小組](/img/create-private-group-4-cropped.png)

新的小組將會放入您的私人小組列表，此列表會顯示您參加的小組

{{< break-do-not-translate >}}

![私人小組裏的對話](/img/private-group-2.png)

私人小組的訊息是以帖子形式組成，任何成員都可回覆訊息或是另開一個新主題帖

小組的創建人才有權力能邀請成員加入，成員可以離開小組，但若是創建人離開則小組即刻解散

{{< break-do-not-translate >}}

# 論壇

![主選單顯示所選論壇功能](/img/nav-drawer-forums.png)

論壇為公開之對話，它不像私人小組，論壇的參加者可邀自己的聯絡人一起加入

要創建論壇，請在主選單下選取"論壇", 即開啟論壇選單。輕觸 + 圖標以建立新論壇

{{< break-do-not-translate >}}

![創建論壇](/img/create-forum-1-cropped.png)

為新論壇命名後, 輕觸 "創建論壇".

{{< break-do-not-translate >}}

![論壇列表顯示最近新建立的論壇](/img/create-forum-2-cropped.png)

新論壇將會被增列入您的論壇列表，此列表會顯示所有您所加入的論壇

{{< break-do-not-translate >}}

![分享論壇, 步驟 1](/img/share-forum-1-cropped.png)

欲向聯絡人分享某論壇，請輕觸該論壇打開它後再按分享圖標

{{< break-do-not-translate >}}

![分享論壇, 步驟 2](/img/share-forum-2-cropped.png)

接下來選取您欲分享的聯絡人，完成後請按下勾選圖標

{{< break-do-not-translate >}}

![分享論壇, 步驟 3](/img/share-forum-3-cropped.png)

向所選的聯絡人送傳一個加選式訊息，然後再輕觸"分享論壇",則被選取的聯絡人將會接到一則邀請訊息

{{< break-do-not-translate >}}

![論壇中的對話](/img/forum-1.png)

論壇的對話訊息是以帖子形式組成，任何成員都可回覆帖子或加開新帖

論壇成員可以自由邀請新人，包括已離開的論壇原創辦人。即便論壇的創辦人離開了，論壇仍會繼續存在直到所有成員都離開。

{{< break-do-not-translate >}}

# 博客

{{< break-do-not-translate >}}

![顯示所選部落格功能選單](/img/nav-drawer-blogs.png)

每個 Briar 帳號都會自帶一個部落格，可用於張貼公開訊息或更新您的最近狀態，您的部落格會自動分享給聯絡人，別人的也是會分享給您

欲讀取聯絡人的部落格或是發佈文章，請在主選單下選取" 部落格"

{{< break-do-not-translate >}}

![撰寫部落格文章, 步驟 1](/img/write-blog-post-1-cropped.png)

要發佈文章，請輕觸上方部落格訊息源中的鋼筆圖標

{{< break-do-not-translate >}}

!撰寫部落格文章, 步驟 2](/img/write-blog-post-2-cropped.png)

撰寫文章再輕點"發佈"

{{< break-do-not-translate >}}

![顯示最近發文的部落格](/img/write-blog-post-3-cropped.png)

您的文章會出現在部落格消息源

{{< break-do-not-translate >}}

![轉載部落格文章, 步驟 1](/img/reblog-1-cropped.png)

要轉載文章，請輕點文章角落處的轉載圖標

{{< break-do-not-translate >}}

![轉載部落格文章, 步驟 2](/img/reblog-2-cropped.png)

增添額外評註再輕按"轉載"

{{< break-do-not-translate >}}

![轉載文章, 步驟 3](/img/reblog-3-cropped.png)

所轉載的文章與您的評論將一併出現在部落格消息源

{{< break-do-not-translate >}}

# RSS 消息源

您可利用 Briar 訂閱瀏覽有 RSS 推送功能的部落格或新聞網站，這些文章都透過Tor 網路下載以確保隱私，您也可以從 RSS 消息源上直接轉載與評論文章。

> **提示: ** RSS 是一種讓網站方便以轉載形式發佈文章的方式

{{< break-do-not-translate >}}

![滙入 RSS 消息源, 步驟 1](/img/import-rss-feed-1-cropped.png)

要滙入RSS 消息源，請自選單下打開部落格消息源並選取 "滙入 RSS 消息源" 

{{< break-do-not-translate >}}

![滙入 RSS 消息源, 步驟 2](/img/import-rss-feed-2-cropped.png)

輸入 RSS 消息源的網址再輕按"滙入"，就可以訂閱部落格消息源並下載其文章，這過程要花點時間。

{{< break-do-not-translate >}}

![部落格消息源顯示最新滙人的 RSS 文章](/img/import-rss-feed-3-cropped.png)

Briar 會自動下載新發佈的文章，所滙入的文章不會主動向您的聯絡人分享，除非您予以轉載

{{< break-do-not-translate >}}

![管理 RSS 消息源, 步驟 1](/img/manage-rss-feeds-1-cropped.png)

要管理 RSS 消息源，請在選單下打開部落格消息源並選取"管理 RSS 消息源"

{{< break-do-not-translate >}}

![管理 RSS 消息源, 步驟 2](/img/manage-rss-feeds-2-cropped.png)

輕觸"清除"圖標即可刪除該消息源，除了您轉載的文章外，所滙入的文章也會自消息源中移除

{{< break-do-not-translate >}}

# 刪除聯絡人 #

![刪除聯絡人](/img/delete-contact-cropped.png)

欲刪除聯絡人，請輕點聯絡人列表上的聯絡人名字再選取主選單中的"刪除聯絡人"

> **提示:** 為保護隱私 刪除聯絡人時對方不會收到通知，此後您在對方設備上都將顯示為離線狀態

{{< break-do-not-translate >}}

# 設置

![設定列表](/img/manual_dark_theme_settings-cropped.png)

要查找設置，請在主選單下選取"設置"
您可以自行定義 Briar 體驗

{{< break-do-not-translate >}}

## 主題

![暗色主題的主選單](/img/manual_dark_theme_nav_drawer-cropped.png)

您可以變動 Briar 使用的配色方案

* **淺色:** Briar 將套用淺色主題
* **深色:** Briar 套用深色主題
* **自動調整:** Briar 將依一天的早晚時間自動變化配色主題
* **系統預設:** Briar 將使用系統預設配色.

{{< break-do-not-translate >}}

## 通過互聯網連接（Tor）

![設置畫面的網路部份](/img/manual_tor_settings-cropped.png)

> **提示:** Briar 透過 Tor 來連接網際網路,  Tor 由全球各地的志工來協助讓使用者可以私密地近用網際網路不必憺心受審查。若政府或網路服務供應商封鎖 Tor, 則"橋接器" 是協助連接 Tor 的電腦。

您可控制 Briar 連接網際網路的方式

* **自動根據地理定位:** Briar 會依您現在的位置來選擇連接方式
* **在沒有橋接器下使用 Tor:** Briar將不使用橋接器來連接 Tor 網路
* **利用橋接器使用 Tor:** Briar使用橋接器來連接 Tor 網路
* **不能連接** Briar 無法連接網際網路

## 使用行動信電傳輸

您可決定 Briar 是否要使用行動數據，若關閉電信傳輸，則只能透過 Wi-Fi 連上網際網路

## 只有在充電時連接到網際網路(Tor)

在設備靠電池供電時，您可決定是否要讓 Briar 使用網際網路，一旦開啟此設定則 Briar 僅在充電時才連接網際網路。

## 螢幕鎖屏

![設定畫面的安全部份](/img/manual_app_lock-cropped.png)

> **提示:**  Android version 4 無法使用此功能

若別人使用您的設備，為為保護隱私您無須登出 Briar 即會自行鎖上。 除非輪人 PIN, 解鎖圖案或密碼，Briar 不能開啟使用。

Briar 使用和解開手機設備的 PIN, 解鎖圖案與密碼一樣，若您未設定 PIN, 解鎖圖案或密碼，則此處將無法設置(呈灰色狀態)，您可透過設備的設置選項來選擇設備上鎖方式。

![主選單顯示 '鎖上應用 ' 選項](/img/manual_app_lock_nav_drawer-cropped.png)

當啟動螢幕鎖定設置，Briar 主選單上將新增一個"鎖上設備"選項，您可利用此選項來鎖住 Briar 而無須登出

{{< break-do-not-translate >}}

![解鎖 Briar](/img/manual_app_lock_keyguard.png)

當 Briar 為鎖上狀態, 需利用 PIN, 解鎖圖案或密碼來打開

{{< break-do-not-translate >}}

## 超過閒置時間螢幕上鎖

> **提示:**  Android version 4 無法使用此功能

若長時間不使用 Briar , 您可選擇讓它自動鎖定。
