---
title: "Používateľská príručka Briar"
---

# Čo je Briar?

Briar je aplikácia na zasielanie správ určená pre aktivistov, novinárov a všetkých, ktorí potrebujú bezpečný, jednoduchý a spoľahlivý spôsob komunikácie. Na rozdiel od tradičných aplikácií na zasielanie správ, Briar sa nespolieha na centrálny server - správy sa synchronizujú priamo medzi zariadeniami používateľov. Ak vypadne internet, Briar sa dokáže synchronizovať cez Bluetooth alebo Wi-Fi, čím udržiava tok informácií aj v krízových situáciách. Ak internet funguje, Briar sa môže synchronizovať cez sieť Tor, čím chráni používateľov a ich vzťahy pred sledovaním.

# Inštalácia

Briar je [k dispozícii na Google Play](https://play.google.com/store/apps/details?id=org.briarproject.briar.android) pre zariadenia so systémom Android.

> **Tip:** Ak si nie ste istí, či máte zariadenie so systémom Android, skontrolujte, či má aplikáciu Obchod Play. Ak áno, používa systém Android.

Ak máte zariadenie so systémom Android, ale nechcete používať službu Google Play, na webovej lokalite Briar nájdete pokyny na inštaláciu aplikácie prostredníctvom [F-Droid](https://briarproject.org/fdroid.html) alebo [priame stiahnutie](https://briarproject.org/apk.html).

# Vytvorenie konta

![Vytvorenie konta](/img/create-account.png)

Pri prvom otvorení aplikácie Briar budete požiadaní o vytvorenie konta. Môžete si vybrať ľubovoľnú prezývku a heslo. Heslo by malo mať aspoň 8 znakov a malo by byť ťažko uhádnuteľné.

> **Upozornenie:** Vaše konto Briar je bezpečne uložené vo vašom zariadení, nie v cloude. Ak odinštalujete aplikáciu Briar alebo zabudnete heslo, konto sa nedá obnoviť.

Ťuknite na položku "Vytvoriť konto". Po vytvorení konta sa dostanete do zoznamu kontaktov.

{{< break-do-not-translate >}}

# Pridávanie kontaktov

![Možnosti pridania kontaktu](/img/add-contact-options-cropped.png)

Ak chcete pridať kontakt, ťuknite na ikonu plus v pravom dolnom rohu zoznamu kontaktov.

Vyberte jednu z dvoch zobrazených možností.

{{< break-do-not-translate >}}

## Pridať vzdialený kontakt 

![Pridanie kontaktu na diaľku](/img/add-contact-remotely.png)

Skopírujte odkaz <code>briar://</code> a pošlite ho osobe, ktorú chcete pridať.
Môžete tiež použiť tlačidlo "Zdieľať" a vybrať aplikáciu na odoslanie odkazu.

Do textového poľa nižšie vložte odkaz, ktorý ste dostali od osoby, ktorú chcete pridať.
Kliknite na tlačidlo "Pokračovať" a vyberte prezývku pre nový kontakt.

Potom sa zobrazí obrazovka "Čakajúce žiadosti o kontakt".
ktorá vás informuje o stave každého čakajúceho kontaktu.
Aplikácia Briar sa bude pravidelne pokúšať pripojiť k vašim kontaktom, aby ich mohol pridať.

Po úspešnom spojení budete navzájom pridaní do zoznamov kontaktov.
Gratulujeme! Ste pripravení na bezpečnú komunikáciu.

Ak sa Briar po 48 hodinách nedokáže pripojiť k vášmu kontaktu, v zozname čakajúcich kontaktov sa zobrazí správa "Pridanie kontaktu zlyhalo". Obaja by ste mali čakajúci kontakt zo zoznamu vymazať a znovu si pridať odkazy.

{{< break-do-not-translate >}}

![Čakajúce kontakty](/img/add-contact-pending-cropped.png)

{{< break-do-not-translate >}}

## Pridať kontakt v blízkosti

![Pridanie kontaktu, krok 1](/img/add-contact-1.png)

Ďalším spôsobom pridania kontaktu je stretnutie s osobou, ktorú chcete pridať. Každý z vás naskenuje QR kód z obrazovky druhej osoby. Tým sa zabezpečí, že sa pripájate k správnej osobe, takže sa za vás nikto iný nemôže vydávať ani čítať vaše správy.

Keď ste pripravení začať, ťuknite na položku "Pokračovať".

{{< break-do-not-translate >}}

![Pridanie kontaktu, krok 2](/img/add-contact-2.png)

Zarovnajte QR kód kontaktu v hľadáčiku. Možno budete musieť počkať niekoľko sekúnd, kým kamera zaostrí.

Keď kamera naskenuje QR kód, zobrazí sa správa "Čaká sa na naskenovanie a pripojenie kontaktu". Teraz by mal váš kontakt naskenovať váš QR kód.

{{< break-do-not-translate >}}

![Zoznam kontaktov zobrazujúci novo pridaný kontakt](/img/contact-list-with-contact-cropped.png)

Vaše zariadenia si vymenia informácie a po niekoľkých sekundách budete pridaní do zoznamov kontaktov. Gratulujeme! Ste pripravení na bezpečnú komunikáciu.

{{< break-do-not-translate >}}

# Posielanie správ ##

![Konverzácia v súkromných správach](/img/manual_messaging-cropped.png)

Ak chcete poslať súkromnú správu, ťuknite na meno kontaktu v zozname kontaktov.

> **Tip:** Všetky správy v aplikácii Briar sú end-to-end šifrované, takže ich nikto iný nemôže čítať.

Ak je váš kontakt offline, vaša správa bude doručená, keď budete obaja online.

{{< break-do-not-translate >}}

# Zoznámenie kontaktov ##

![Spustenie zoznámenia](/img/introduction-1-cropped.png)

Svoje kontakty môžete navzájom zoznámiť prostredníctvom aplikácie Briar. To im umožní stať sa kontaktmi bez toho, aby sa museli stretnúť. Ak chcete začať zoznamovanie, ťuknite na meno kontaktu v zozname kontaktov a z ponuky vyberte položku "Zoznámiť)".

{{< break-do-not-translate >}}

![Výber druhého kontaktu](/img/introduction-2-cropped.png)

Ďalej vyberte druhý kontakt, ktorý chcete predstaviť.

{{< break-do-not-translate >}}

![Pridanie správy obom kontaktom](/img/introduction-3-cropped.png)

Ku kontaktom pridajte voliteľnú správu a potom ťuknite na položku "Zoznámiť".

{{< break-do-not-translate >}}

![Žiadosť o zoznámenie](/img/introduction-5-cropped.png)

Vašim kontaktom sa zobrazí správa s otázkou, či súhlasia so zoznámením. Ak obaja súhlasia, budú navzájom pridaní do zoznamu kontaktov a budú môcť bezpečne komunikovať.

{{< break-do-not-translate >}}

# Súkromné skupiny ##

![Hlavné menu zobrazujúce vybranú funkciu súkromných skupín](/img/nav-drawer-private-groups.png)

Briar môžete používať na skupinové konverzácie s kontaktmi. Ak chcete vytvoriť skupinu, otvorte hlavné menu a vyberte položku "Súkromné skupiny". Otvorí sa zoznam súkromných skupín. Klepnutím na ikonu plus vytvoríte novú skupinu.

{{< break-do-not-translate >}}

![Vytvorenie súkromnej skupiny, krok 1](/img/create-private-group-1-cropped.png)

Vyberte názov skupiny a potom ťuknite na položku "Vytvoriť skupinu".

{{< break-do-not-translate >}}

![Vytvorenie súkromnej skupiny, krok 2](/img/create-private-group-2-cropped.png)

Ďalej vyberte kontakty, ktoré chcete pozvať do skupiny. Po dokončení stlačte ikonu označenia.

{{< break-do-not-translate >}}

![Vytvorenie súkromnej skupiny, krok 3](/img/create-private-group-3-cropped.png)

Pridajte voliteľnú správu vybraným kontaktom a potom ťuknite na položku "Poslať pozvánku". Kontaktom sa zobrazí správa s pozvánkou na pripojenie.

{{< break-do-not-translate >}}

![Zoznam súkromných skupín zobrazujúci novo vytvorenú skupinu](/img/create-private-group-4-cropped.png)

Nová skupina sa pridá do vášho zoznamu súkromných skupín. V tomto zozname sa zobrazujú všetky skupiny, do ktorých patríte.

{{< break-do-not-translate >}}

![Konverzácia v súkromnej skupine](/img/private-group-2.png)

Správy v súkromných skupinách sú usporiadané do vlákien. Každý člen môže odpovedať na správu alebo založiť nové vlákno.

Pozvať nových členov môže len zakladateľ skupiny. Každý člen môže skupinu opustiť. Ak odíde tvorca, skupina sa zruší.

{{< break-do-not-translate >}}

# Fóra

![Hlavná ponuka zobrazujúca vybranú funkciu fóra](/img/nav-drawer-forums.png)

Fórum je verejná diskusia. Na rozdiel od súkromnej skupiny môže každý, kto sa pripojí k fóru, pozvať svoje vlastné kontakty.

Ak chcete vytvoriť fórum, otvorte hlavnú ponuku a vyberte položku "Fóra". Otvorí sa zoznam fór. Ťuknutím na ikonu plus vytvoríte nové fórum.

{{< break-do-not-translate >}}

![Vytvorenie fóra](/img/create-forum-1-cropped.png)

Vyberte názov fóra a potom ťuknite na položku "Vytvoriť fórum".

{{< break-do-not-translate >}}

![Zoznam fór zobrazujúci novo vytvorené fórum](/img/create-forum-2-cropped.png)

Nové fórum sa pridá do vášho zoznamu fór. V tomto zozname sú zobrazené všetky fóra, do ktorých patríte.

{{< break-do-not-translate >}}

![Zdieľanie fóra, krok 1](/img/share-forum-1-cropped.png)

Ak chcete zdieľať fórum so svojimi kontaktmi, ťuknutím na fórum ho otvorte a potom ťuknite na ikonu zdieľania.

{{< break-do-not-translate >}}

![Zdieľanie fóra, krok 2](/img/share-forum-2-cropped.png)

Ďalej vyberte kontakty, s ktorými chcete zdieľať fórum. Po dokončení stlačte ikonu označenia.

{{< break-do-not-translate >}}

![Zdieľanie fóra, krok 3](/img/share-forum-3-cropped.png)

Pridajte voliteľnú správu vybraným kontaktom a potom ťuknite na položku "Zdieľať fórum". Kontaktom sa zobrazí správa s výzvou na pripojenie.

{{< break-do-not-translate >}}

![Konverzácia na fóre](/img/forum-1.png)

Správy vo fórach sú usporiadané do vlákien. Každý člen môže odpovedať na správu alebo založiť nové vlákno.

Každý člen fóra môže pozvať nových členov a každý člen vrátane zakladateľa môže fórum opustiť. Fórum existuje dovtedy, kým ho neopustí posledný člen.

{{< break-do-not-translate >}}

# Blogy

{{< break-do-not-translate >}}

![Hlavná ponuka zobrazujúca vybranú funkciu blogov](/img/nav-drawer-blogs.png)

Vaše konto Briar má zabudovaný blog. Môžete ho používať na uverejňovanie noviniek a aktualizácií o svojom živote. Váš blog sa automaticky zdieľa s vašimi kontaktmi a ich blogy sa zdieľajú s vami.

Ak si chcete prečítať blogy svojich kontaktov alebo napísať príspevok, otvorte hlavné menu a vyberte položku "Blogy".

{{< break-do-not-translate >}}

![Napísanie príspevku na blog, krok 1](/img/write-blog-post-1-cropped.png)

Ak chcete napísať príspevok, ťuknite na ikonu pera v hornej časti kanála blogu.

{{< break-do-not-translate >}}

![Napísanie príspevku na blog, krok 2](/img/write-blog-post-2-cropped.png)

Napíšte príspevok a ťuknite na položku " Zverejniť".

{{< break-do-not-translate >}}

![Kanál blogu zobrazujúci novo vytvorený príspevok](/img/write-blog-post-3-cropped.png)

Váš nový príspevok sa zobrazí v kanáli blogu.

{{< break-do-not-translate >}}

![Opätovné zdieľanie príspevku na blogu, krok 1](/img/reblog-1-cropped.png)

Ak chcete opätovne zdieľať príspevok, ťuknite na ikonu opätovného pridania príspevku v rohu príspevku.

{{< break-do-not-translate >}}

![Opätovné zdieľanie príspevku na blogu, krok 2](/img/reblog-2-cropped.png)

Pridajte nepovinný komentár a ťuknite na položku " Znovu zdieľať".

{{< break-do-not-translate >}}

![Opätovné zdieľanie príspevku na blogu, krok 3](/img/reblog-3-cropped.png)

Znovu zdieľaný príspevok sa zobrazí v kanáli blogu s vaším komentárom.

{{< break-do-not-translate >}}

# Kanály RSS

Pomocou aplikácie Briar môžete čítať akýkoľvek blog alebo spravodajskú stránku, ktorá zverejňuje kanál RSS. Na ochranu vášho súkromia sa články sťahujú cez Tor. Články z kanálov RSS môžete opätovne pridávať a komentovať, rovnako ako príspevky na blogu.

> **Tip:** RSS je spôsob, ako webové stránky môžu uverejňovať články vo forme, ktorá sa dá ľahko opakovane zverejniť.

{{< break-do-not-translate >}}

![Importovanie kanála RSS, krok 1](/img/import-rss-feed-1-cropped.png)

Ak chcete importovať kanál RSS, otvorte kanál blogu a z ponuky vyberte položku "Importovať RSS kanál".

{{< break-do-not-translate >}}

![Importovanie kanála RSS, krok 2](/img/import-rss-feed-2-cropped.png)

Zadajte adresu URL RSS kanála a ťuknite na položku "Importovať". Články sa stiahnu a pridajú do kanála blogu. Môže to trvať niekoľko minút.

{{< break-do-not-translate >}}

![Kanál blogu zobrazujúci novo importovaný RSS článok](/img/import-rss-feed-3-cropped.png)

Keď sa uverejnia nové články, aplikácia Briar ich automaticky stiahne. Importované články sa nezdieľajú s vašimi kontaktmi, pokiaľ sa nerozhodnete ich opätovne pridať.

{{< break-do-not-translate >}}

![Spravovanie kanálov RSS, krok 1](/img/manage-rss-feeds-1-cropped.png)

Ak chcete spravovať kanály RSS, otvorte kanál blogu a z ponuky vyberte položku "Spravovať RSS kanály".

{{< break-do-not-translate >}}

![Spravovanie kanálov RSS, krok 2](/img/manage-rss-feeds-2-cropped.png)

Ťuknutím na ikonu koša odstránite kanál. Importované články sa odstránia z kanála blogu, okrem všetkých článkov, ktoré ste opätovne pridali.

{{< break-do-not-translate >}}

# Odstránenie kontaktov #

![Odstránenie kontaktu](/img/delete-contact-cropped.png)

Ak chcete odstrániť kontakt, ťuknite na jeho meno v zozname kontaktov a z ponuky vyberte položku "Odstrániť kontakt".

> **Tip:** V záujme ochrany vášho súkromia nebude kontakt upozornený na to, že ste ho odstránili. Odteraz vás budú vidieť len ako offline.

{{< break-do-not-translate >}}

# Nastavenia

![Zoznam nastavení](/img/manual_dark_theme_settings-cropped.png)

Ak chcete nájsť nastavenia, otvorte hlavné menu a vyberte položku "Nastavenia".
Tu si môžete prispôsobiť svoje nastavenia aplikácie Briar.

{{< break-do-not-translate >}}

## Téma

![Hlavná ponuka v tmavej téme](/img/manual_dark_theme_nav_drawer-cropped.png)

Môžete zmeniť farebnú schému, ktorú používa Briar:

* **Svetlá:** Briar bude používať svetlé farby.
* **Tmavá:** Briar bude používať tmavé farby.
* **Automatická:** Briar bude meniť svoju farebnú schému na základe dennej doby.
* **Predvolené nastavenie systému:** Briar bude používať systémovú farebnú schému.

{{< break-do-not-translate >}}

## Pripojiť sa cez internet (Tor)

![Sekcia "Siete" na obrazovke nastavení](/img/manual_tor_settings-cropped.png)

> **Tip:** Briar používa Tor na pripojenie k internetu. Tor je sieť počítačov prevádzkovaná dobrovoľníkmi na celom svete, ktorá pomáha ľuďom pristupovať k internetu súkromne a bez cenzúry. "Mosty" sú počítače, ktoré vám pomôžu pripojiť sa na Tor, ak ho vaša vláda alebo poskytovateľ internetu blokujú.

Môžete ovládať, ako sa Briar pripája k internetu:

* **Automaticky na základe polohy:** Briar vyberie spôsob pripojenia na základe vašej aktuálnej polohy.
* **Použiť Tor bez mostov:** Briar sa pripojí k Toru bez použitia mostov.
* **Použiť Tor s mostami:** Briar použije na pripojenie k Tor mosty.
* **Nepripájať sa:** Briar sa vôbec nepripojí k internetu.

## Použiť mobilné dáta

Môžete ovládať, či Briar využíva mobilné dáta. Ak vypnete mobilné dáta, Briar bude používať internet len vtedy, keď budete pripojení k Wi-Fi.

## Pripojiť sa cez internet (Tor) len pri nabíjaní

Môžete ovládať, či aplikácia Briar má používať internet, keď zariadenie ide iba batériu. Ak zapnete toto nastavenie, Briar bude používať internet len vtedy, keď bude vaše zariadenie pripojené k napájaniu.

## Zámok obrazovky

![Časť "Zabezpečenie" na obrazovke nastavení](/img/manual_app_lock-cropped.png)

> **Tip:** Táto funkcia nie je dostupná na systéme Android verzie 4.

Ak chcete chrániť svoje súkromie, keď vaše zariadenie používajú iné osoby, môžete Briar uzamknúť bez odhlásenia. Tým sa zabráni používaniu aplikácie Briar, kým nezadáte PIN kód, vzor alebo heslo.

Briar používa na odomknutie zariadenia ten istý PIN kód, vzor alebo heslo, ktoré bežne používate, takže toto nastavenie bude vypnuté (zasivené), ak ste si ešte nezvolili PIN kód, vzor alebo heslo. Na ich výber môžete použiť aplikáciu Nastavenia zariadenia.

![Hlavná ponuka zobrazujúca možnosť "Zamknúť aplikáciu"](/img/manual_app_lock_nav_drawer-cropped.png)

Po aktivácii nastavenia uzamknutia obrazovky sa do hlavnej ponuky aplikácie Briar pridá možnosť "Uzamknúť aplikáciu". Túto možnosť môžete použiť na uzamknutie aplikácie Briar bez odhlásenia.

{{< break-do-not-translate >}}

![Odomknutie aplikácie Briar](/img/manual_app_lock_keyguard.png)

Keď je aplikácia Briar uzamknutá, budete požiadaní o zadanie PIN kódu, vzoru alebo hesla na jej odomknutie.

{{< break-do-not-translate >}}

## Časový limit nečinnosti zámku obrazovky

> **Tip:** Táto funkcia nie je dostupná na systéme Android verzie 4.

Môžete si zvoliť automatické uzamknutie aplikácie Briar, keď sa určitý čas nepoužíva.
